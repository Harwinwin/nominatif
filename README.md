# Aplikasi Immobi Tracker

Project React ada di folder `client/` dan di root folder ada server Adonis.

Script yang bisa digunakan untuk berinteraksi dapat dilihat di dalam `package.json` di root folder

### Beberapa script yang dapat digunakan:
1. `npm run server` -- start server Adonis
2. `npm run client` -- start server React
3. `npm run dev` -- start server Adonis dan React bersamaan
4. `npm run client-install` -- install package dependency React
5. `npm run server-install` -- install package dependency Adonis

Untuk bergabung dalam project, pastikan telah melakukan instalasi package React dan Adonis dengan menjalankan `npm run client-install` & `npm run server-install`
