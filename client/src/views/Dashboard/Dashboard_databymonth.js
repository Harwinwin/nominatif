import React, { Component } from "react"
import { ResponsivePie } from '@nivo/pie'
import { Card, CardBody, CardHeader } from "reactstrap"
// import { Skeleton } from '@material-ui/lab';
import "./DashboardCard.scss"
// import dashboardBackground from "./background_dashboard.png"

class DataMonthCard extends Component {

    render() {
        return (
            <React.Fragment>
                <Card className={`dashboard-card dc-blue`}>
                    <CardHeader className="p-3">
                        <h5 className="avenir-black-primary">Data Rekap Bulanan</h5>
                    </CardHeader>
                    <CardBody>
                            <ResponsivePie
                                data={this.props.data}
                                margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                                innerRadius={0.5}
                                padAngle={0.7}
                                cornerRadius={3}
                                colors={{ scheme: 'nivo' }}
                                borderWidth={1}
                                borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
                                radialLabelsSkipAngle={10}
                                radialLabelsTextXOffset={6}
                                radialLabelsTextColor="#333333"
                                radialLabelsLinkOffset={0}
                                radialLabelsLinkDiagonalLength={16}
                                radialLabelsLinkHorizontalLength={24}
                                radialLabelsLinkStrokeWidth={1}
                                radialLabelsLinkColor={{ from: 'color' }}
                                slicesLabelsSkipAngle={10}
                                slicesLabelsTextColor="#333333"
                                animate={true}
                                motionStiffness={90}
                                motionDamping={15}
                                legends={[
                                    {
                                        anchor: 'bottom',
                                        direction: 'row',
                                        translateY: 56,
                                        itemWidth: 100,
                                        itemHeight: 18,
                                        itemTextColor: '#999',
                                        symbolSize: 18,
                                        symbolShape: 'circle',
                                        effects: [
                                            {
                                                on: 'hover',
                                                style: {
                                                    itemTextColor: '#000'
                                                }
                                            }
                                        ]
                                    }
                                ]}
                            />
                    </CardBody>
                </Card>
            </React.Fragment>
        )
    }
}

export default DataMonthCard
