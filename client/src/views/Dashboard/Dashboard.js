import React from "react"
import { makeStyles } from "@material-ui/core/styles";
// import Icon from "@material-ui/core/Icon";
// import GridItem from "../../components/Grid/GridItem.js";
// import GridContainer from "../../components/Grid/GridContainer.js";
// import Card from "../../components/Card/Card";
// import CardHeader from "../../components/Card/CardHeader.js";
// import CardIcon from "../../components/Card/CardIcon.js";
// import CardBody from "../../components/Card/CardBody.js";
// import CardFooter from "../../components/Card/CardFooter.js";
import dashboardBackground from "./background_dashboard.png"

import styles from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
const useStyles = makeStyles(styles);
export default function Dashboard() {
  const classes = useStyles();
  return (
    <div className="dashboard" align="center">
      <img src={dashboardBackground} width='50%' position='left' />
    </div>

    // <div >
    //   <GridContainer>
    //     <GridItem xs={12} sm={6} md={3}>
    //       <Card>
    //         <CardHeader color="warning" stats icon>
    //           <CardIcon color="warning">
    //             {/* <Icon>content_copy</Icon> */}
    //           </CardIcon>
    //           <p className={classes.cardCategory}>Used Space</p>
    //           <h3 className={classes.cardTitle}>
    //             49/50 <small>GB</small>
    //           </h3>
    //         </CardHeader>
    //         <CardFooter stats>
    //           <div className={classes.stats}>
    //             {/* <Danger>
    //                     <Warning />
    //                   </Danger> */}
    //             <a href="#pablo" onClick={e => e.preventDefault()}>
    //               Get more space
    //                   </a>
    //           </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //     <GridItem xs={12} sm={6} md={3}>
    //       <Card>
    //         <CardHeader color="success" stats icon>
    //           <CardIcon color="success">
    //             {/* <Store /> */}
    //           </CardIcon>
    //           <p className={classes.cardCategory}>Revenue</p>
    //           <h3 className={classes.cardTitle}>$34,245</h3>
    //         </CardHeader>
    //         <CardFooter stats>
    //           <div className={classes.stats}>
    //             {/* <DateRange /> */}
    //             Last 24 Hours
    //                 </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //     <GridItem xs={12} sm={6} md={3}>
    //       <Card>
    //         <CardHeader color="info" stats icon>
    //           <CardIcon color="info">
    //             {/* <Icon>info_outline</Icon> */}
    //           </CardIcon>
    //           <p className={classes.cardCategory}>Fixed Issues</p>
    //           <h3 className={classes.cardTitle}>75</h3>
    //         </CardHeader>
    //         <CardFooter stats>
    //           <div className={classes.stats}>
    //             {/* <LocalOffer /> */}
    //             Tracked from Github
    //                 </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //     <GridItem xs={12} sm={6} md={3}>
    //       <Card>
    //         <CardHeader color="info" stats icon>
    //           <CardIcon color="info">
    //             {/* <Accessibility /> */}
    //           </CardIcon>
    //           <p className={classes.cardCategory}>Followers</p>
    //           <h3 className={classes.cardTitle}>+245</h3>
    //         </CardHeader>
    //         <CardFooter stats>
    //           <div className={classes.stats}>
    //             {/* <Update /> */}
    //             Just Updated
    //           </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //   </GridContainer>
    //   <GridContainer>
    //     <GridItem xs={12} sm={12} md={4}>
    //       <Card chart>
    //         <CardHeader color="success">

    //         </CardHeader>
    //         <CardBody>
    //           <h4 className={classes.cardTitle}>Daily Sales</h4>
    //           <p className={classes.cardCategory}>
    //             <span className={classes.successText}>
    //               {/* <ArrowUpward className={classes.upArrowCardCategory} />  */}
    //               55%
    //                   </span>{" "}
    //             increase in today sales.
    //                 </p>
    //         </CardBody>
    //         <CardFooter chart>
    //           <div className={classes.stats}>
    //             {/* <AccessTime />  */}
    //             updated 4 minutes ago
    //           </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //     <GridItem xs={12} sm={12} md={4}>
    //       <Card chart>
    //         <CardHeader color="warning">
    //           {/* <ChartistGraph
    //                   className="ct-chart"
    //                   data={emailsSubscriptionChart.data}
    //                   type="Bar"
    //                   options={emailsSubscriptionChart.options}
    //                   responsiveOptions={emailsSubscriptionChart.responsiveOptions}
    //                   listener={emailsSubscriptionChart.animation}
    //                 /> */}
    //         </CardHeader>
    //         <CardBody>
    //           <h4 className={classes.cardTitle}>Email Subscriptions</h4>
    //           <p className={classes.cardCategory}>Last Campaign Performance</p>
    //         </CardBody>
    //         <CardFooter chart>
    //           <div className={classes.stats}>
    //             {/* <AccessTime />  */}
    //             campaign sent 2 days ago
    //                 </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //     <GridItem xs={12} sm={12} md={4}>
    //       <Card chart>
    //         <CardHeader color="danger">

    //         </CardHeader>
    //         <CardBody>
    //           <h4 className={classes.cardTitle}>Completed Tasks</h4>
    //           <p className={classes.cardCategory}>Last Campaign Performance</p>
    //         </CardBody>
    //         <CardFooter chart>
    //           <div className={classes.stats}>
    //             {/* <AccessTime />  */}
    //             campaign sent 2 days ago
    //                 </div>
    //         </CardFooter>
    //       </Card>
    //     </GridItem>
    //   </GridContainer>
    // </div>
  )

}

// export default Dashboard
