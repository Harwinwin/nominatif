import React, { Component } from "react"
import Button from "@material-ui/core/Button"
import ClickAwayListener from "@material-ui/core/ClickAwayListener"
import Grow from "@material-ui/core/Grow"
import Paper from "@material-ui/core/Paper"
import Popper from "@material-ui/core/Popper"
//import MenuItem from "@material-ui/core/MenuItem"
import MenuList from "@material-ui/core/MenuList"
//import TextField from "@material-ui/core/TextField"
import { withStyles } from "@material-ui/core/styles"

const styles = theme => ({
  searchBtn: {
    padding: "3px 10px",
    fontSize: 10,
    fontWeight: "bold",
    marginLeft: 5
  },
  paperWidth: {
    width: "20rem"
  }
})

class FilterSearch extends Component {
  state = {
    isOpen: false,
    searched: ""
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  handleClose = e => {
    if (this.anchorEl.contains(e.target)) {
      return
    }

    this.setState({ isOpen: false })
  }

  render() {
    const { classes } = this.props

    return (
      <div className="filter-bar">
        <Button
          className="filters"
          buttonRef={node => {
            this.anchorEl = node
          }}
          onClick={this.toggle}>
          <i style={{ color: "#007bff" }} className="fa fa-search " />{" "}
          <span style={{ margin: "0 5px"}}>Search</span> <i className="fa fa-angle-down" />
        </Button>
        <Popper
          open={this.state.isOpen}
          anchorEl={this.anchorEl}
          transition
          disablePortal
          placement="bottom-end">
          {({ TransitionProps }) => (
            <Grow {...TransitionProps} style={{ transformOrigin: "center bottom" }}>
              <Paper className={classes.paperWidth}>
                <ClickAwayListener onClickAway={this.handleClose}>
                  <MenuList>
                    <input
                      style={{
                        background: "white",
                        border: "solid 0.5px #d8d8d8",
                        borderRadius: 7,
                        margin: "0 10px",
                        width: "70%"
                      }}
                    />
                    <Button
                      variant="contained"
                      color="primary"
                      className={`${classes.searchBtn} btn-primary`}
                      size="small">
                      <span style={{ color: "#fff" }}>Search</span>
                    </Button>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    )
  }
}

export default withStyles(styles)(FilterSearch)
