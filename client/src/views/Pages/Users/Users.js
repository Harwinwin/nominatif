import React, { Component } from "react"
import UserTable from "./UserTable_new"
import UsersFilter from "./UsersFilter"
import CircularProgress from "@material-ui/core/CircularProgress"


class Users extends Component {
  state = {
    users: null,
  }


  //ini untuk fetch datanya usahain sama route nya sama kaya di start/route.js, kalo ada .prefix tambahin api/v1
 async componentDidMount(){
    await fetch("api/v1/user/userviewall")
    .then(resp => {
      console.log(resp.status, resp.statusText)
      return resp.json()
    })
    .then(users =>{
      
      this.setState({users : users[0]})
    })
    .catch(err => console.log(err))

    console.log(this.state.users);
  } 

  render() {
    const { users } = this.state
    return (
      <>
        <UsersFilter  />
        {users ? (
          <UserTable
          users={this.state.users}
        />
        ) : (
          <CircularProgress style={{ marginLeft: 10, color: "#007bff" }} />
        )}
        
      </>
    )
  }
}

export default Users
