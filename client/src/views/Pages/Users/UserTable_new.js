import React from "react";
import MUIDataTable from "mui-datatables";
import { createMuiTheme } from "@material-ui/core/styles"
import EditUser from "./EditUser"


class UserTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      response: null,
      isDialogOpen : false,
      userSelected : null
    }
    this.tableState = {};
    this.handleTableInit = this.handleTableInit.bind(this);
    this.handleTableChange = this.handleTableChange.bind(this);
  }

  setID = async (event, ID) => {
    await console.log(ID);
    await localStorage.setItem("userID", ID);
    //await localStorage.setItem("no",projectID)
    this.props.history.push("/detailUser")
  }
 
  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUITableHead: {
        root: {
          boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
        
        },
      },
      MUIDataTableHeadCell: {
        root: {
          
          whiteSpace: 'nowrap',
        },
      },
      MUIDataTableBodyCell: {
        root: {
          whiteSpace: 'nowrap'
        }
      }
    }
  });

  handleTableInit = (action, tableState) => {
    console.log('handleTableInit: ', tableState);
    this.tableState = tableState;
  };

  handleTableChange = (action, tableState) => {
    this.tableState = tableState;
  };


  async handleDialogOpen(value) {
    await Promise.all([fetch(`api/v1/user/viewbyid/${value}`)])
      .then(([detailUserResp]) => {
        return Promise.all([detailUserResp.json()])
      })
      .then(([detailUser]) => {
        this.setState({ detailUser: detailUser[0], userSelected : value })
        console.log( "detailUser" , detailUser[0])
      })
      .catch(error => console.log(error))
      this.setState({ isDialogOpen: true })
  }

  handleDialogClose = () => {
    this.setState({ isDialogOpen: false })
  }

  async handledelete(value) {

    var form = {
      ID: value,
    }

    console.log(form)

    const url = "api/v1/user/deleteduser"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));

    if (this.state.response.status === 'success') {
      this.setState({
        response: null,
      })
      window.location.reload();
    }
  }

  render() {
    const { users } = this.props
    const{ detailUser, userSelected } = this.state
    const columns = [
      {
        label: "Role",
        name: "Group_Name",
        options: {
          filter: true,
          
        }
      },
      {
        name: "Name",
        label: "Name",
        options: {
          filter: true,
        }
      },
      {
        name: "Email",
        label: "Email",
        options: {
          filter: true,
        }
      },
      {
        name: "Divisi",
        label:"Divisi",
        options: {
          filter: true,
          sort: false
        }
      },
      {
        name: "",
        label: "Action",
        options: {
          filter: false,
          sort: false,
          print: false,
          download: false,
          customBodyRender: (value) => {
              return (
                <div>
                  <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                    <i onClick={() => this.handledelete(value)} className={`fa fa-trash`} />
                  </span>
                </div>
              );
            }
          } 
      },
      {
        name: "",
        label:"Edit",
        options: {
          filter: false,
          sort: false,
          print: false,
          customBodyRender: (value) => {
            return ( <i onClick={() => this.handleDialogOpen(value)}
                className={`fa fa-pencil-square-o`}
              />
            );
            }
        }
      },    
    ];

    const data = users.map(u =>[
       `${u.Role}`,`${u.Name}`,`${u.Email}`,`${u.divisi_name}`,`${u.ID}`,`${u.ID}`
    ])

    const options = {
      filter: true,
      filterType: 'multiselect',
      responsive: 'scroll',
      elevation: 0,
      onTableInit: this.handleTableInit,
      onTableChange: this.handleTableChange,
    };

    return (
        <div>
          <div>
          <MUIDataTable data={data} columns={columns} options={options} />
        </div>
        <EditUser
            detailUser={detailUser}
            userSelected={userSelected}
            handleDialogClose={this.handleDialogClose}
            isDialogOpen={this.state.isDialogOpen}           
            />    
        </div>
    );
  }
}

// export default withStyles(styles)(UserTable)
export default UserTable