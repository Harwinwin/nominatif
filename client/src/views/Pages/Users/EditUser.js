import React, { Component } from "react"
import { Row, Col, Label, Button } from "reactstrap"
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import "./addNewUser.scss"


const variantIcon = {
  success: CheckCircleIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: '#007bff',
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: "1px solid #007bff",
    margin: 0,
    padding: 10
  },
  closeButton: {
    position: "absolute",
    right: 5,
    top: 5,
    color: "#007bff"
  }
}))(props => {
  const { children, classes, onClose } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
        {children}
      </span>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

export default class AddNewUsers extends Component {
  constructor(props) {
    super(props);
    //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
    this.state = {
        role: "",
        name: '',
        password: '',
        email: '',
        divisi: "",
        response:null,
        open :false,
        userSelected : null,
        detailUser : null,
        listDivisi : null,
        listRole : null
    };
}

async componentDidMount(){
  await Promise.all([fetch("api/v1/user/getData")])
  .then(([userResp]) => {
    return Promise.all([userResp.json()])
  })
  .then(([listData]) => {
    this.setState({ listRole: listData.role,
       listDivisi: listData.divisi})
    console.log( 
    "role : " ,
     listData.divisi,
     "divisi : ",
     listData.role)
  })
  .catch(error => console.log(error))

}

async componentWillReceiveProps(nextProps){
    const detailUser = nextProps.detailUser;
    const userSelected = nextProps.userSelected;


    if(userSelected !== null ){
      await this.setState({userSelected : userSelected});
    }

    console.log( "detailUserProps", detailUser)
    if(detailUser !== null || detailUser !==  undefined){
     await this.setState({
      role: detailUser[0].Role,
      name: detailUser[0].Name,
      email: detailUser[0].Email,
      divisi: detailUser[0].divisi
    });
     console.log(this.state.detailUser)
    }
    else{
      return "No data"
    }
  }

handleChange = (e) => {
  const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
  this.setState({
      [name]: value
  })
}


//fungsi add user
onSubmit = async  (e) => {
 e.preventDefault();
 await this.setState({response : null});

//kan kalo pake postman di bodynya pake json, nah dibuat dlu jsonnya ini caranya
var form = {
        Role: parseInt(this.state.role),
        Name: this.state.name,
        Password: this.state.password,
        Email: this.state.email,
        Divisi: parseInt(this.state.divisi),
        userId: this.props.userSelected
      }

  //ini urlnya liat di start/route.js , tanya siapa yang buat controllernya
  console.log(form)
  const url = "api/v1/user/edituser"

  await fetch(url, { method: "POST",
    body: JSON.stringify(form),
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    } })
    .then(res => {
      return res.json()})
    .then(response => {
      this.setState({response})})
    .catch(error => console.error('Error:', error));
  
    if(this.state.response.status === 'success'){
      this.setState({open:true})
      this.setState ({
        role: null,
        name: '',
        password: '',
        email: '',
        divisi: '',
        response:null,
        detailUser : null,
        userSelected : null
      })
      window.location.reload()
    }
  
  }


  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  handleInit() {
    console.log('FilePond instance has initialised', this.pond);
  }

  render() {
    const { isDialogOpen, handleDialogClose } = this.props
    //usahain pake avform yak di npm install dlu terus uda tinggal liat aja di paling atas yang ada import AvFormnya
    return (
      <>
      {this.state.listDivisi && this.state.listRole? (
      <>
        <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
       <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={this.state.open}
        autoHideDuration={6000}
        onClose={this.handleClose}
      >
        <MySnackbarContentWrapper
          onClose={this.handleClose}
          variant="success"
          message="User Created"
        />
      </Snackbar>
      <DialogTitle onClose={handleDialogClose}>Edit User</DialogTitle>
      <DialogContent>
      <AvForm onValidSubmit={(e) => this.onSubmit(e)}>
      <Row style={{ margin: "1.5rem 3rem" }} className="add-new-users">
          <Col md="6" style={{ padding: "0.25rem " }}>
            <AvGroup>
              <Label className="avenir-black-primary" for="role">
                Role
              </Label>
              <AvInput value={this.state.role} onChange={e=>this.handleChange(e)} type="select" name="role" id="user-role">
                <option value="">Select Role</option>
                {this.state.listRole.map(r => (
                <option value={r.id}>{r.role_name}</option>
                ))}
              </AvInput>
              <AvFeedback>Select Role required</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label className="avenir-black-primary">Full Name</Label>
              <AvInput type="text" name="name" value={this.state.name} onChange={e=>this.handleChange(e)} placeholder="Input full name" />
              <AvFeedback>Name required</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label className="avenir-black-primary">New Password</Label>
              <AvInput type="password" value={this.state.password} onChange={e=>this.handleChange(e)} name="password" placeholder="Input password" />
              <AvFeedback>Password required</AvFeedback>
            </AvGroup>
          </Col>
          <Col md="6" style={{ padding: "0.25rem" }}>
            <AvGroup>
              <Label className="avenir-black-primary">Email</Label>
              <AvInput type="email" value={this.state.email} onChange={e=>this.handleChange(e)} name="email" placeholder="Input email" />
              <AvFeedback>E-mail required</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label className="avenir-black-primary" for="divisi">
                Divisi
              </Label>
              <AvInput value={this.state.divisi} onChange={e=>this.handleChange(e)} type="select" name="divisi" id="divisi-selected">
                <option value = "" >Select Divisi</option>
                {this.state.listDivisi.map(r => (
                <option value={r.id}>{r.divisi_name}</option>
                ))}
              </AvInput>
              <AvFeedback>Select Divisi required</AvFeedback>
            </AvGroup>
            <Button
              type="submit"
              color="primary"
              className="px-4"
              style={{ fontSize: 18, position: "flex", bottom: 0, right: 0, marginTop: "10%" }}
              
              >
              Save
            </Button>

          </Col>
        </Row>
      </AvForm>
      </DialogContent>
    </Dialog>
        
      </>

      ) : (
        <div></div>


      )}
    
    </>
    )
  }
}
