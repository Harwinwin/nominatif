import React, { Component } from "react"
import Button from "@material-ui/core/Button"
import ClickAwayListener from "@material-ui/core/ClickAwayListener"
import Grow from "@material-ui/core/Grow"
import Paper from "@material-ui/core/Paper"
import Popper from "@material-ui/core/Popper"
import MenuItem from "@material-ui/core/MenuItem"
import MenuList from "@material-ui/core/MenuList"
import Divider from "@material-ui/core/Divider"
import Checkbox from "@material-ui/core/Checkbox"
import { withStyles } from "@material-ui/core/styles"

const styles = theme => ({
  filterMenu: {
    padding: "5px 10px",
    fontSize: "0.75rem"
  }
})

class FilterFilter extends Component {
  state = {
      isOpen: false,
      filterAdmin: false,
      filterBA: false,
      filterClient: false,
      filterDesigner: false,
      filterDevDB: false,
      filterDevSniffy: false,
      filterDevWeb: false,
      filterDeveloper: false,
      filterDeveloperBA: false,
      filterITSupport: false,
      filterManager: false,
      filterProjectAdmin: false,
      filterProjectManager: false,
      filterSME: false,
      filterTester: false
  }

  toggle = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  handleClose = e => {
    if (this.anchorEl.contains(e.target)) {
      return
    }

    this.setState({ isOpen: false })
  }

  handleChange = e => {
    this.setState({ [e.target.name]: !this.state[e.target.name] })
  }

  render() {
    const {
      isOpen,
      filterAdmin,
      filterBA,
      filterClient,
      filterDesigner,
      filterDevDB,
      filterDevSniffy,
      filterDevWeb,
      filterDeveloper,
      filterDeveloperBA,
      filterITSupport,
      filterManager,
      filterProjectAdmin,
      filterProjectManager,
      filterSME,
      filterTester
    } = this.state

    const { classes } = this.props

    return (
      <div className="filter-bar">
        <Button
          className="filters"
          buttonRef={node => {
            this.anchorEl = node
          }}
          onClick={this.toggle}>
          Filter <i className="fa fa-angle-down" style={{ margin: 10 }} />
        </Button>
        <Popper open={isOpen} anchorEl={this.anchorEl} transition disablePortal>
          {({ TransitionProps }) => (
            <Grow {...TransitionProps} style={{ transformOrigin: "center bottom" }}>
              <Paper>
                <ClickAwayListener onClickAway={this.handleClose}>
                  <MenuList>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterAdmin"
                        checked={filterAdmin}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Admin
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterBA"
                        checked={filterBA}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      BA
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterClient"
                        checked={filterClient}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Client
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDesigner"
                        checked={filterDesigner}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Designer
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDevDB"
                        checked={filterDevDB}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Dev - DB
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDevSniffy"
                        checked={filterDevSniffy}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Dev - Sniffy
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDevWeb"
                        checked={filterDevWeb}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Dev - Web
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDeveloper"
                        checked={filterDeveloper}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Developer
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterDeveloperBA"
                        checked={filterDeveloperBA}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Developer BA
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterITSupport"
                        checked={filterITSupport}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      IT Support
                      </MenuItem>
                      <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterManager"
                        checked={filterManager}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Manager
                    </MenuItem>
                      <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterProjectAdmin"
                        checked={filterProjectAdmin}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      ProjectAdmin
                    </MenuItem>
                      <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterProjectManager"
                        checked={filterProjectManager}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Project Manager
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterSME"
                        checked={filterSME}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      SME
                    </MenuItem>
                    <MenuItem className={classes.filterMenu}>
                      <Checkbox
                        className="filter-checkbox"
                        name="filterTester"
                        checked={filterTester}
                        onChange={this.handleChange}
                        color="primary"
                      />
                      Tester
                    </MenuItem>
                    <Divider component="li" />
                    <MenuItem
                      className={`filters-label ${classes.filterMenu}`}
                      onClick={this.handleClose}>
                      <i className="fa fa-angle-up" style={{ color: "#c3c3c3" }} /> Filter By Selected
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    )
  }
}

export default withStyles(styles)(FilterFilter)
