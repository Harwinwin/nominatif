import React, { Component } from "react"
import { Button } from "reactstrap"
import { withStyles } from '@material-ui/core/styles';
import AddNewUsers from "./AddNewUsers"
  
  const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 0,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
      outline: 'none',
    },
  });

class UsersFilter extends Component {
    state = {
        isModalOpen: false
    }

    handleDialogOpen = () => {
        this.setState({ isDialogOpen: true });
      };
    
      handleDialogClose = () => {
        this.setState({ isDialogOpen: false });
      };
    

    render() {
    
        return (
            <div className="filter-container">
            <Button color="primary" className="btn-add-new" size="sm" onClick={this.handleDialogOpen}>
                <i className="fa fa-plus" style={{ marginRight: 5, fontSize: 10 }} /> Add New Users
            </Button>
            <AddNewUsers
              handleDialogClose={this.handleDialogClose}
              isDialogOpen={this.state.isDialogOpen}
            />
            </div>
        )
    }
}

export default withStyles(styles)(UsersFilter)