import React from "react"
import { withStyles } from "@material-ui/core/styles"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import Radio from "@material-ui/core/Radio"
import Avatar from "@material-ui/core/Avatar"
import userIcon from "../../../assets/img/dashboard/user@2x.png"
// import "./Users.scss"
//import { Checkbox } from "@material-ui/core";

const styles = theme => ({
  avatar: {
    margin: 10,
    width: 30,
    height: 30
  },
  iconBluish: {
    color: "#007bff"
  },
  iconTrash: {
    fontSize: 15
  },
  iconEdit: {
    marginRight: 10,
    fontSize: 15
  },
  tableHead: {
    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)"
  },
  tableCell: {
    paddingRight: 25,
    paddingLeft: 25
  },
  tableCellTight: {
    paddingRight: 8,
    paddingLeft: 8
  },
  tableCellHeader: {
    paddingRight: 25,
    paddingLeft: 25,
    fontSize: 14
  }
})

const UsersTable = props => {
  const { users, handleSelected, classes } = props

  const selected = typeof props.selected === "string" ? parseInt(props.selected) : props.selected // Type-check

  return (
    <Table>
      <TableHead className={classes.tableHead}>
        <TableRow>
          <TableCell className={classes.tableCellTight}>
            {/* Table Checkbox: Custom overriding for MUI svg element */}
            <Radio disabled className="table-checkbox" />
          </TableCell>
          <TableCell className={classes.tableCellTight} />
            <TableCell className={classes.tableCellHeader}>ID</TableCell>
            <TableCell className={classes.tableCellHeader}>Group</TableCell>
            <TableCell className={classes.tableCellHeader}>Name</TableCell>
            <TableCell className={classes.tableCellHeader}>Email</TableCell>
            <TableCell className={classes.tableCellHeader}>Phone</TableCell>
            <TableCell className={classes.tableCellHeader}>Active</TableCell>
            <TableCell className={classes.tableCellHeader} />
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(d => (
            <TableRow key={d.ID}>
              <TableCell className={classes.tableCellTight}>
                <Radio
                  color="primary"
                  checked={selected === d.ID ? true : false}
                  onChange={handleSelected}
                  value={d.ID}
                  className="table-checkbox"
                />
              </TableCell>
              <TableCell className={classes.tableCellTight}>
                <Avatar alt="User" src={userIcon} className={classes.avatar} />
              </TableCell>
              <TableCell className={classes.tableCell} component="th" scope="row">
                {d.ID}
              </TableCell>
              <TableCell className={classes.tableCell}>{d.Group_Name}</TableCell>
              <TableCell className={classes.tableCell}>{d.Name}</TableCell>
              <TableCell className={classes.tableCell}>{d.Email}</TableCell>
              <TableCell className={classes.tableCell}>{d.phone}</TableCell>
              <TableCell className={classes.tableCell}>{d.active}</TableCell>
              <TableCell className={classes.tableCellTight}>
                <i
                  className={`fa fa-pencil-square-o ${classes.iconEdit} ${selected === d.ID &&
                    classes.iconBluish}`}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    )
  }

export default withStyles(styles)(UsersTable)
