import React, { Component } from "react"
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { isMobile } from 'react-device-detect'
import { CircularProgress } from "@material-ui/core";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Label,
  Row
} from "reactstrap"

import loginBackground from "./backgroundlogin.png"
import loginBackgroundMb from "./backgroundlogin.jpg"
import "./Login.scss"

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      amount: '',
      response:null,
      email: '',
      password: '',
      weight: '',
      weightRange: '',
      showPassword: false,
      remember: null,
      isLoading: false
    };
  }

  async componentDidMount(){
    if (await localStorage.getItem('token')) return this.props.history.push("/dashboard")
  }

  handleChange = (e) => {
    const target = e.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
    this.setState({
        [name]: value
    })
  }

  handleLogin = async event => {
    this.setState({isLoading:!this.state.isLoading})
    const url = `api/v1/login`

    const form = await {
      email : this.state.email,
      password : this.state.password
    }

    await fetch(url, { method: "POST",
    body: JSON.stringify(form),
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    } })
    .then(res => {
      return res.json()})
    .then(response => {
      this.setState({response})})
    .catch(error => console.error('Error:', error));
    
    console.log(this.state.response)
    if(this.state.response.token){
       Promise.all([localStorage.setItem('user', JSON.stringify(this.state.response)), localStorage.setItem('token', this.state.response.token) ])
                .then(() => {
                  this.props.history.push("/dashboard")
                })
    }else{
      this.setState({isLoading:!this.state.isLoading})
      window.alert("Login Error")
    }
   
  }

  sendEmail = () => {
    this.setState({ isOpen: false })
    this.props.history.push("/login")
  }

  render() {
    const { isLoading } = this.state
    return (
      <div className="app flex-row align-items-center login-background">
        <img className="login-background" src ={isMobile? loginBackgroundMb:loginBackground}/>
        <Container>
          <Row className="justify-content-center">
            <Col className = "row-login" md="5">
              <Card className="p-5 login-card">
                <CardBody>
                  <h4 style={{color:"#007bff", fontWeight:'bold', position:'relative', bottom:'0.5rem', left:'-0.1rem'}}>{isMobile?"":"Login"}</h4>
                  <AvForm onValidSubmit={(e) => this.handleLogin(e)} className="login-form" method="POST">
                    <AvGroup>
                      <Label for="email">Email</Label>
                      <AvInput required value={this.state.email} onChange={e => this.handleChange(e)} type="email" name="email" id="email" placeholder="Insert Email" />
                    </AvGroup>
                    <AvGroup>
                      <Label for="password">Password</Label>
                      <AvInput
                        required
                        value = {this.state.password}
                        onChange={e => this.handleChange(e)} 
                        type="password"
                        name="password"
                        id="adornment-password"
                        placeholder="Input your password"
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="Toggle password visibility">
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                    </AvGroup>

                    <Row>
                      <Col xs="6" className="text-left">  
                      </Col>
                      <Col xs="6">
                        <Button
                          color="primary"
                          className="float-right px-4"
                          type="submit">
                          {isLoading ? <CircularProgress size={25} color={"white"}/> : "Login" }
                        </Button>
                      </Col>
                    </Row>

                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Login
