import React from "react"
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from "reactstrap"
import { Redirect } from "react-router-dom"

const ForgotPassword = props => {
  const closeBtn = (
    <button className="close" onClick={props.toggle}>
      &times;
    </button>
  )

  return (
    <Modal isOpen={props.isOpen} toggle={props.toggle} className={props.className} centered={true}>
      <ModalHeader toggle={props.toggle} close={closeBtn}>
        <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
          Forgot Password
        </span>
      </ModalHeader>
      <ModalBody>
        <Form className="login-form" onSubmit={props.sendEmail}>
          <FormGroup>
            <Label for="email">Enter Your Email Address Below</Label>
            <Input type="email" name="email" id="email" placeholder="immobi@gmail.com" />
          </FormGroup>
          <Button color="primary float-right mt-3 mb-3">Send New Password</Button>
        </Form>
      </ModalBody>
    </Modal>
  )
}

export default ForgotPassword
