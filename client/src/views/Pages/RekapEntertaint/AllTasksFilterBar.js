import React, { Component } from "react"
import { Button } from "reactstrap";
import { withRouter } from 'react-router-dom';

class AllTasksFilterBar extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false
    };
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  handlePrintAll = async () =>{
    await localStorage.setItem("exportdata", JSON.stringify(this.props.data));
    this.props.history.push({
      pathname: '/exportallentertaint',
      
    })
  }

  showBtn = () => {
      return(
        <div>
         <Button onClick={this.handlePrintAll} color="primary" className="btn-add-new" size="sm" >
                <i className="fa fa-print" style={{ marginRight: 5, fontSize: 10 }} /> Print Data
          </Button>

        </div> 
      )
  }


  render() {
    return (
      <div className="filter-container" style={{position:"absolute",top:'4.7rem' , right:'3rem'}}>
         {this.showBtn()}
      </div>
    )
  }
}

export default withRouter(AllTasksFilterBar)
