import React from "react";
import MUIDataTable from "mui-datatables";
import CustomToolbarSelect from "./CustomSelectToolbar";
import { createMuiTheme, MuiThemeProvider, withStyles } from "@material-ui/core/styles"
import { FormGroup, FormLabel, TextField } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab'
import { withRouter } from 'react-router-dom';
import TablePagination from "@material-ui/core/TablePagination";
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import EditEntertaint from "./EditEntertaint"

const styles = theme => ({
  iconBluish: {
    color: "#007bff"
  },
  iconEdit: {
    marginRight: 10,
    fontSize: 15
  },
  check: {
    color: "#007bff",
    fontSize: 15
  },
  tableHead: {
    boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)"
  },
  tableCell: {
    paddingRight: 25,
    paddingLeft: 25
  },
  tableCellTight: {
    paddingRight: 8,
    paddingLeft: 8
  },
  tableCellHeader: {
    paddingRight: 25,
    paddingLeft: 25,
    fontSize: 14
  }
})


class TasksTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isDialogOpen: false,
      isModalOpen: false,
      role: null,
      rp: null,
      response: null,
      datatables: null,
      selectedIdEntertaint: null,
      detailEntertaint: null
    }

    this.handleTableInit = this.handleTableInit.bind(this);
    this.handleTableChange = this.handleTableChange.bind(this);
  }


  async handleDialogOpen(value) {
    console.log(value)
    await Promise.all([fetch(`api/v1/entertaint/getentertaintbyid/${value}`)])
      .then(([detailEntertaintResp]) => {
        return Promise.all([detailEntertaintResp.json()])
      })
      .then(([detailEntertaint]) => {
        this.setState({ detailEntertaint: detailEntertaint[0], selectedIdEntertaint: value })
        console.log("detailEntertaintedits", detailEntertaint[0])
      })
      .catch(error => console.log(error))
    this.setState({ isDialogOpen: true })
  }

  handleDialogClose = () => {
    this.setState({ isDialogOpen: false })
  }

  async componentDidMount() {
    var users = await JSON.parse(localStorage.getItem("user"))
    await this.setState({ role: users.role })
  }

  setdatatables = (value) => {
    this.setState({ datatables: value })
  }

  setID = async (event, no) => {
    await console.log(no);
    await localStorage.setItem("entertaintID", no);
    this.props.history.push("/detailEntertaint")
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUITableHead: {
        root: {
          boxShadow: "0 3px 30px 0 rgba(139, 139, 139, 0.16)",
        },
      },
      MUIDataTableHeadCell: {
        root: {
          whiteSpace: 'nowrap',
        },
      },
      MUIDataTableBodyCell: {
        root: {
          whiteSpace: 'nowrap'
        }
      },

    }
  });

  async handlechangeStatus(id, value) {

    var form = {
      Status: value,
      ID: id,
    }
    const url = "api/v1/entertaint/changestatus"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));

    if (this.state.response.status === 'success') {
      this.setState({
        response: null,
      })
      window.location.reload();
    }
  }

  async handledelete(value) {

    var form = {
      ID: value,
    }

    console.log(form)

    const url = "api/v1/entertaint/deletedentertaint"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));

    if (this.state.response.status === 'success') {
      this.setState({
        response: null,
      })
      window.location.reload();
    }
  }

  handleTableInit = (action, tableState) => {
    var array = []
    array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
    console.log(array)
    this.props.setDataprint(array)
    var duit = []
    array[0].map(d => duit.push(d[10]))
    // console.log(duit)
    var total_nominal = 0
    duit.map(item => total_nominal = parseInt((item.slice(3, -3)).split('.').join('')) + total_nominal)
    // console.log("total", total_nominal)

    var formatter = new Intl.NumberFormat('id', {
      style: 'currency',
      currency: 'IDR',
    });

    this.setState({ rp: formatter.format(total_nominal) })
  };

  handleTableChange = (action, tableState) => {
    var array = []
    array.push(tableState.displayData.map(ts => ts.data.map(d => d)))
    var duit = []
    console.log(array)
    this.props.setDataprint(array)
    array[0].map(d => duit.push(d[10]))
    // console.log(duit)
    var total_nominal = 0
    duit.map(item => total_nominal = parseInt((item.slice(3, -3)).split('.').join('')) + total_nominal)
    // console.log("total", total_nominal)

    var formatter = new Intl.NumberFormat('id', {
      style: 'currency',
      currency: 'IDR',
    });

    this.setState({ rp: formatter.format(total_nominal) })
  };

  columns = [
    {
      name: "Tanggal",
      label: "Tanggal Entertaint",
      options: {
        filter: true,
        filterType: 'custom',
        customFilterListRender: v => {
          if (v['min'] && v['max']) {
            return `Start Date: ${v['min']}, End Date: ${v['max']}`;
          } else if (v['min']) {
            return `Start Date: ${v['min']}`;
          } else if (v['max']) {
            return `End Date: ${v['max']}`;
          }
          return false;
        },
        filterOptions: {
          names: [],
          logic(tanggal, filters) {
            var tgl = new Date(tanggal)
            var max = new Date(filters['max'])
            var min = new Date(filters['min'])
            if (filters['min'] && filters['max']) {
              return tgl < min || tgl > max;
            } else if (filters['min']) {
              return tgl < min;
            } else if (filters['max']) {
              return tgl > max;
            }
            return false;
          },
          display: (filterList, onChange, index, column) => (
            <div>
              <FormLabel>Tanggal</FormLabel>
              <FormGroup row>
                <TextField
                  label="Start Date"
                  type="date"
                  value={filterList[index]['min'] || ''}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={event => {
                    filterList[index]['min'] = event.target.value;
                    onChange(filterList[index], index, column);
                  }}
                  style={{ width: '45%', marginRight: '5%' }}
                />
                <TextField
                  label="End Date"
                  type="date"
                  value={filterList[index]['max'] || ''}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={event => {
                    filterList[index]['max'] = event.target.value;
                    onChange(filterList[index], index, column);
                  }}
                  style={{ width: '45%' }}
                />
              </FormGroup>
            </div>
          ),
        },
      }
    },
    {
      name: "jenis Entertaint",
      id: "jenis",
      label: "Jenis Entertaint",
      options: {
        filter: false,
        sort: true
      },
    },
    {
      name: "PIC Entertaint",
      label: "PIC Entertaint(User)",
      options: {
        filter: true
      }
    },
    {
      name: "Divisi",
      label: "Divisi",
      options: {
        filter: true

      }
    },
    {
      name: "Pengeluaran Via",
      label: "Pengeluaran Via",
      options: {
        filter: false
      }
    },
    {
      name: "Advance Settlement/Reimbursement No",
      label: "Advance Settlement/Reimbursement No",
      options: {
        filter: false

      }
    },
    {
      name: "Periode Corporate CC",
      label: "Periode Corporate CC",
      options: {
        filter: false
      }
    },
    {
      name: "Nama Proyek",
      label: "Nama Proyek",
      options: {
        filter: false
      }
    },
    {
      name: "Nama Penerima Entertaint",
      label: "Nama Penerima Entertaint",
      options: {
        filter: false
      }
    },
    {
      name: "Nama Perusahaan Penerima Entertaint",
      label: "Nama Perusahaan Penerima Entertaint",
      options: {
        filter: false
      }
    },
    {
      name: "Nominal Entertaint",
      label: "Nominal Entertaint(Rp)",
      options: {
        filter: false,
        customBodyRender: (value) => {
          var formatter = new Intl.NumberFormat('id', {
            style: 'currency',
            currency: 'IDR',
          });

          return formatter.format(value)

        }
      }
    },
    {
      name: "Status",
      label: "Status",
      options: {
        filter: true
      }
    },
    {
      name: "",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        print: false,
        download: false,
        customBodyRender: (value) => {
          var users = JSON.parse(localStorage.getItem("user"))
          if (value[1] === 1) {
            if (users.role === 1 || users.role === 3) {
              console.log("jalan")
              return (
                <div>
                  <span style={{ marginLeft: '-5px', fontSize: '20px', color: 'green' }}>
                    <i onClick={() => this.handlechangeStatus(value[0], "approved")} className={`fa fa-check-square`} />
                  </span>
                  <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                    <i onClick={() => this.handlechangeStatus(value[0], "rejected")} className={`fa fa-window-close`} />
                  </span>
                  <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                    <i onClick={() => this.handledelete(value[0])} className={`fa fa-trash`} />
                  </span>
                </div>
              );
            } else if (users.role === 2) {
              return (
                <div>
                  <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                    <i onClick={() => this.handledelete(value[0])} className={`fa fa-trash`} />
                  </span>
                </div>
              );
            }
          } else if (value[1] === 3) {
            return (
              <div>
                <span style={{ marginLeft: '5px', fontSize: '20px', color: 'red' }}>
                  <i onClick={() => this.handledelete(value[0])} className={`fa fa-trash`} />
                </span>
              </div>
            );
          }
        },
      }
    },
    {
      name: "edit",
      label: "",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (value, tableMeta, updateValue) => {
          if (value[1] === 1) {
            return (
              <i onClick={() => this.handleDialogOpen(value[0])} className={`fa fa-pencil-square-o ${this.props.classes.iconEdit}`} />
            );
          } else {
            return <div></div>
          }
        }
      }
    },
  ];


  data = this.props.rekap.map(r => [
    `${r.date}`,
    `${r.jenis}`,
    `${r.pic}`,
    `${r.Divisi}`,
    `${r.pengeluaran}`,
    `${r.no_trx}`,
    `${r.periode_cc}`,
    `${r.projek_name}`,
    `${r.penerima}`,
    `${r.perusahaan}`,
    parseInt(`${r.nominal}`),
    `${r.status_name}`,
    [parseInt(`${r.id}`),
    parseInt(`${r.status}`)],
    [parseInt(`${r.id}`),
    parseInt(`${r.status}`)]
  ])

  options = {
    filter: true,
    print: false,
    filterType: 'multiselect',
    pagination: true,
    elevation: 0,
    rowsPerPage: 10,
    responsive: 'stacked',
    onTableInit: this.handleTableInit,
    onTableChange: this.handleTableChange,
    download: false,
    customSort: (data, colIndex, order) => {
      return data.sort((a, b) => {
        console.log(colIndex)
        if (colIndex === 0) {
          var a = new Date(a.data[colIndex])
          var b = new Date(b.data[colIndex])
          return (a < b ? -1 : 1) * (order === 'desc' ? 1 : -1);
        } else {
          return (
            (a.data[colIndex] < b.data[colIndex] ? -1 : 1) * (order === 'desc' ? 1 : -1)
          );
        }

      });
    },

    downloadOptions: {
      filename: 'dataRekapEntertain.csv',
    }, customToolbarSelect: (selectedRows, displayData, setSelectedRows) => (
      <CustomToolbarSelect selectedRows={selectedRows} displayData={displayData} setSelectedRows={setSelectedRows} />
    ),
    customFooter: (
      count,
      page,
      rowsPerPage,
      changeRowsPerPage,
      changePage
    ) => {
      return (
        <TableFooter>
          <TableRow>
            <TableCell align='left'>
              <TablePagination
                rowsPerPageOptions={[10, 25, 50]}
                component="div"
                count={count}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onChangePage={(_, page) => changePage(page)}
                onChangeRowsPerPage={event => changeRowsPerPage(event.target.value)}
              />
            </TableCell>
            <TableCell align='right' style={{ fontSize: '15px', color: 'black' }}>
              {`Grand Total Nominal Entertaint = ${this.state.rp}`}
            </TableCell>
          </TableRow>
        </TableFooter>

      );
    }
  };



  render() {
    const { detailEntertaint } = this.state

    return (
      <>
        {this.data && this.columns ? (
          <>
            <div>
              <MuiThemeProvider theme={this.getMuiTheme()}>
                <MUIDataTable data={this.data} columns={this.columns} options={this.options} />
              </MuiThemeProvider>
              <div>
                <EditEntertaint
                  detailEntertaint={detailEntertaint}
                  handleDialogClose={this.handleDialogClose}
                  isDialogOpen={this.state.isDialogOpen}
                  selectedIdEntertaint={this.state.selectedIdEntertaint}
                />
              </div>
            </div>
          </>
        ) : (
            <div></div>
          )}

      </>

    );
  }
}

export default withRouter(withStyles(styles)(TasksTable))
