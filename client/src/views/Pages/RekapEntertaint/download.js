import React from "react"
import TableDownload from './TableDownload'


export default class ExportAll extends React.Component {
    constructor(props){
        super(props);

        this.state={
            rekapSelected : null
        }
    }

    async componentDidMount(){
        var printdata = await JSON.parse(localStorage.getItem("dataSelected"));
        await this.setState({ rekapSelected: printdata })
        console.log(this.state.rekapSelected)

    }

    render() {

        return (
            <>
                {this.state.rekapSelected ?(
                    <>
                         <TableDownload printData = {this.state.rekapSelected}/>
                    </>
                ) :
                
                    <div>

                    </div>
                }
            </>
                    
          
        )
    }


}



