import React from "react"
import { withStyles } from "@material-ui/core/styles"
import { withRouter } from 'react-router-dom';
import ReactToExcel from 'react-html-table-to-excel';

const styles = theme => ({
    avatar: {
        margin: 10,
        width: 30,
        height: 30
    },
    iconBluish: {
        color: "#007bff"
    },
    iconGreenCircle: {
        color: "#07e364",
        marginRight: 5
    },
    // iconTrash: {
    //   fontSize: 15
    // },
    iconEdit: {
        marginRight: 10,
        fontSize: 15
    },
    tableCell: {
        padding: 10,
        fontSize: 12,
        whiteSpace: 'nowrap',
        border: '1px solid'
    },
    tableCellTight: {
        width: 70,
        paddingRight: 5,
        paddingLeft: 5
    },
    tableCellHeader: {
        padding: 5,
        fontSize: 12,
        color: 'black',
        border: '1px solid',
        fontWeight: '1000'
    }
})



class TablePrint extends React.PureComponent {
    render() {
        var total_nominal = 0;
        this.props.printData.map((item) => {
            total_nominal = parseInt((item[11].slice(3, -3)).split('.').join('')) + total_nominal
        })

        var formatter = new Intl.NumberFormat('id', {
            style: 'currency',
            currency: 'IDR',
        });

        var rp = formatter.format(total_nominal)


        const { classes } = this.props
        const { printData } = this.props
        return (
            <>
                {printData ? (
                    <>
                        <div>

                            <div pageStyle="@page { size: A4 landscape;}" size="small" ref={el => (this.componentRef = el)} style={{ width: '100%', padding: 25 }}>
                                <h5>PT IMMOBI SOLUSI PRIMA</h5>
                                <h5 style={{ marginBottom: '10%' }}>Daftar Nominatif Entertaint</h5>
                                <table id="table-to-xls">
                                    <thead className={classes.tableHead}>

                                        <th className={classes.tableCellHeader}>No</th>
                                        <th className={classes.tableCellHeader}>Tanggal Entertaint</th>
                                        <th className={classes.tableCellHeader}>Jenis Entertaint</th>
                                        <th className={classes.tableCellHeader}>PIC Entertaint</th>
                                        <th className={classes.tableCellHeader}>Divisi</th>
                                        <th className={classes.tableCellHeader}>Pengeluaran via</th>
                                        <th className={classes.tableCellHeader}>No. Reimbursement/Advance Settlement </th>
                                        <th className={classes.tableCellHeader}>Periode Corporate CC </th>
                                        <th className={classes.tableCellHeader}>Nama Proyek </th>
                                        <th className={classes.tableCellHeader}>Nama Penerima Entertaint </th>
                                        <th className={classes.tableCellHeader}>Nama Penerima Perusahaan Entertaint </th>
                                        <th align={"center"} className={classes.tableCellHeader}>Nominal (Rp.) </th>

                                    </thead>

                                    <tbody>
                                        {printData.map((p, i) => (
                                            <tr>
                                                <td className={classes.tableCell}>{i + 1}</td>
                                                <td className={classes.tableCell}>{p[1]}</td>
                                                <td className={classes.tableCell}>{p[2]}</td>
                                                <td className={classes.tableCell}>{p[3]}</td>
                                                <td className={classes.tableCell}>{p[4]}</td>
                                                <td className={classes.tableCell}>{p[5]}</td>
                                                <td className={classes.tableCell}>{p[6]}</td>
                                                <td className={classes.tableCell}>{p[7] === "null" ? "" : p[7]}</td>
                                                <td className={classes.tableCell}>{p[8]}</td>
                                                <td className={classes.tableCell}>{p[9]}</td>
                                                <td className={classes.tableCell}>{p[10]}</td>
                                                <td className={classes.tableCell}>{p[11]}</td>
                                            </tr>
                                        ))}
                                        <tr>
                                            <td style={{ fontWeight: 1000 }} className={classes.tableCell} align={"center"} colSpan={'11'}>Grand Total</td>
                                            <td style={{ fontWeight: 1000 }} className={classes.tableCell}>{rp}</td>
                                        </tr>

                                    </tbody>
                                </table>



                            </div>

                            <ReactToExcel
                            className="btn btn-primary"
                            table="table-to-xls"
                            filename="rekapentertaint"
                            sheet="sheet 1"
                            buttonText="EXPORT TO EXCEL"
                            />

                        </div>
                    </>
                ) : (
                        <h1>Tidak ada data</h1>
                    )}

            </>

        )
    }


}


export default withRouter(withStyles(styles)(TablePrint))
