import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import PrintIcon from "@material-ui/icons/Print";
import IndeterminateCheckBoxIcon from "@material-ui/icons/IndeterminateCheckBox";
import DownloadIcon from "@material-ui/icons/Archive";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from 'react-router-dom';

const defaultToolbarSelectStyles = {
  iconButton: {
  },
  iconContainer: {
    marginRight: "24px",
  },
  inverseIcon: {
    transform: "rotate(90deg)",
  },
};

class CustomToolbarSelect extends React.Component {
  handleClickInverseSelection = async () => {
    var selectedData= []
    await this.props.selectedRows.data.map(row => selectedData.push(this.props.displayData[row.index].data))
    console.log(selectedData)
   await localStorage.setItem("dataSelected", JSON.stringify(selectedData));
    this.props.history.push({
      pathname: '/exportselected',
    })
  };

  handleClickDeselectAll = () => {
    this.props.setSelectedRows([]);
  };

  handleClickBlockSelected = async () => {
    var selectedData= []
    await this.props.selectedRows.data.map(row => selectedData.push(this.props.displayData[row.index].data))
    console.log(selectedData)
   await localStorage.setItem("dataSelected", JSON.stringify(selectedData));
    this.props.history.push({
      pathname: '/downloadselected',
    })
  
   
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.iconContainer}>
        <Tooltip title={"Deselect ALL"}>
          <IconButton className={classes.iconButton} onClick={this.handleClickDeselectAll}>
            <IndeterminateCheckBoxIcon className={classes.icon} />
          </IconButton>
        </Tooltip>
        <Tooltip title={"Print Selected"}>
          <IconButton className={classes.iconButton} onClick={this.handleClickInverseSelection}>
            <PrintIcon className={classes.icon} />
          </IconButton>
        </Tooltip>
        <Tooltip title={"Download selected"}>
          <IconButton className={classes.iconButton} onClick={this.handleClickBlockSelected}>
            <DownloadIcon className={classes.icon} />
          </IconButton>
        </Tooltip>
      </div>
    );
  }
}

export default withRouter(withStyles(defaultToolbarSelectStyles)(CustomToolbarSelect));