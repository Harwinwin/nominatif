import React, { Component } from "react"
import RekapTable from "./AllTasksTable_New"
import RekapBar from "./AllTasksFilterBar"
import { Skeleton } from '@material-ui/lab'

export default class RekapEntertaint extends Component{
  state = {
    selected: -1,
    rekap: null,
    dataPrint : null
  }

  async componentDidMount(){
    var users = await JSON.parse(localStorage.getItem("user"))
    await fetch(`api/v1/entertaint/getdata/${users.id}/${users.role}/${users.divisi}`, 
    {headers: {
      "Content-Type": "application/json",
      "Authorization" : `Bearer ${users.token}`
    }})
    .then(resp => {
      console.log(resp.status, resp.statusText)
      return resp.json()
    })
    .then(rekap =>{
      
      this.setState({rekap : rekap[0]})
    })
    .catch(err => console.log(err))

    console.log(this.state.rekap);
  }

  handleSelected = e => {
    this.setState({ selected: e.target.value })
  }

  setDataprint = (value) =>{
    this.setState({dataPrint : value})
  }

  render() {
    const {rekap} = this.state
    return (
      <>
        <RekapBar data={this.state.dataPrint} />
        {rekap ? (
          <RekapTable
          rekap={this.state.rekap}
          setDataprint={this.setDataprint}
        />
        ) : (
          <Skeleton  variant="rect" />
        )}
      </>
    )
  }
}