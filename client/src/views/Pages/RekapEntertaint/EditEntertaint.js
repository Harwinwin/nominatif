import React, { Component } from "react"
import { Label } from "reactstrap"
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import Button from '@material-ui/core/Button'
import { withStyles } from "@material-ui/core/styles"
import Dialog from "@material-ui/core/Dialog"
import DialogContent from "@material-ui/core/DialogContent"
import MuiDialogTitle from "@material-ui/core/DialogTitle"
import IconButton from "@material-ui/core/IconButton"
import CloseIcon from "@material-ui/icons/Close"
import _ from 'lodash'

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: "1px solid #007bff",
    margin: 0,
    padding: 10
  },
  closeButton: {
    position: "absolute",
    right: 5,
    top: 5,
    color: "#007bff"
  }
}))(props => {
  const { children, classes, onClose } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <span className="avenir-black-primary ml-4 mb-0" style={{ fontSize: "24px" }}>
        {children}
      </span>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

export default class EditEntertaint extends Component {

  setRedirect = () => {
    window.location.reload()
  }

  constructor(props) {
    super(props);
    this.state = {
      tgl: null,
      users: null,
      jenis: null,
      other: null,
      pengeluaran: null,
      advance: null,
      projectname: null,
      picpenerima: null,
      penerima: null,
      enter0: null,
      enter1: null,
      enter2: null,
      enter3: null,
      enter4: null,
      namaperusahaan: null,
      vialabel: "",
      nominal: null,
      hidden: "none",
      hidden1: "none",
      submitName: "",
      cc: "",
      isRequired: false,
      selectedIdEntertaint: null,
      detailEntertaint: null
    };
  }

  async componentWillReceiveProps(nextProps) {
    const detailEntertaint = _.isEqual(nextProps.detailEntertaint, this.props.detailEntertaint);

    const selectedIdEntertaint = _.isEqual(nextProps.selectedIdEntertaint, this.props.selectedIdEntertaint);

    console.log(detailEntertaint)

    var array = ["", "Jamuan", "Golf", "Karaoke", "Gift"]

    if (!detailEntertaint) {
      await this.setState({
        detailEntertaint: nextProps.detailEntertaint[0], tgl: nextProps.detailEntertaint[0].date,
        jenis: nextProps.detailEntertaint[0].jenis,penerima: nextProps.detailEntertaint[0].penerima.includes(",") ? nextProps.detailEntertaint[0].penerima.split(",") : nextProps.detailEntertaint[0].penerima.split(" "),
        pengeluaran: nextProps.detailEntertaint[0].pengeluaran, advance: nextProps.detailEntertaint[0].no_trx,
        cc: nextProps.detailEntertaint[0].periode_cc, namaperusahaan: nextProps.detailEntertaint[0].perusahaan,
        nominal: nextProps.detailEntertaint[0].nominal, projectname: nextProps.detailEntertaint[0].projek_name
      });

      var flag = 0

      for(var i = 0 ; i < this.state.penerima.length ; i++){
        console.log(this.state.penerima.length)
        switch(i){
          case 0 : 
          console.log("0")
          this.setState({enter0 : this.state.penerima[0]});
          this.setState({enter1: "",
            enter2: "",
            enter3: "",
            enter4: ""})
          break;
          case 1 : 
          console.log("1")
          this.setState({enter1 : this.state.penerima[1]})
          break;
          case 2 : 
          console.log("2")
          this.setState({enter2 : this.state.penerima[2]})
          break;
          case 3 : 
          console.log("3")
          this.setState({enter3 : this.state.penerima[3]})
          break;
          case 4 : 
          console.log("4")
          this.setState({enter4 : this.state.penerima[4]})
          break;
        }
      }

      for (var i = 0; i < array.length; i++) {
        if (this.state.jenis !== array[i]) {
          flag++
        }
      }

      console.log(flag)

      if (flag === 5) {
        console.log("jalan")
        this.setState({ other: this.state.jenis })
        this.setState({ jenis: "Lain"})
      }
    }

    await console.log(this.state.penerima)
    await console.log(this.state.enter0);

    if (!selectedIdEntertaint) {
      await this.setState({ selectedIdEntertaint: nextProps.selectedIdEntertaint });
    }

  }

  async handleenter1Change(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if (name === "jenis") {
      if (target.value === "Lain") {
        this.setState({ hidden: "" })
      }
      else {
        this.setState({ hidden: "none" })
      }
    } else if (name === "pengeluaran") {
      console.log(target.value)
      if (target.value === "") {
        this.setState({ hidden1: "none" })
      } else if (target.value === "Corporate Credit Card") {
        await this.setState({ hidden1: "", vialabel: target.value, isRequired: 'enable' })
        console.log(this.state.isRequired)
      } else {
        this.setState({ hidden1: "", vialabel: target.value, isRequired: false })
      }
    }

    this.setState({
      [name]: value
    })
  }

  async componentDidMount() {
    var users = await JSON.parse(localStorage.getItem("user"));
    this.setState({ picpenerima: users.id })
    await fetch("api/v1/user/myaccount/" + users.id)
      .then(resp => {
        console.log(resp.status, resp.statusText)
        return resp.json()
      })
      .then(users => {

        this.setState({ users: users[0][0] })
      })
      .catch(err => console.log(err))

    console.log(this.state.users);
    if (this.props.enter1) {
      this.setState({ enter1: this.props.enter1 });
    }
  }

  componentWillUnmount(){
    this.setState({
      tgl: null,
      users: null,
      jenis: null,
      other: null,
      pengeluaran: null,
      advance: null,
      projectname: null,
      picpenerima: null,
      penerima: null,
      enter0: null,
      enter1: null,
      enter2: null,
      enter3: null,
      enter4: null,
      namaperusahaan: null,
      vialabel: "",
      nominal: null,
    })
  }

  setSubmit = (e, value) => {
    if (value === "print") {
      this.props.history.push({
        pathname: '/exportentertaint',
      })
    } else {
      this.setState({ submitName: value })
    }
  }

  handleFormSubmit = async (e, value) => {
    const { selectedIdEntertaint } = this.state;
    e.preventDefault()
    await this.setState({ response: null });

    var penerima = []
    if (this.state.enter0 !== null) {
      console.log(this.state.enter0)
      penerima.push(this.state.enter0)
    }

    if (this.state.enter1 !== null || this.state.enter1 !== "null") {
      console.log(this.state.enter1)
      penerima.push(this.state.enter1)
    }

    if (this.state.enter2 !== null || this.state.enter2 !== "null") {
      console.log(this.state.enter2)
      penerima.push(this.state.enter2)
    }

    if (this.state.enter3 !== null || this.state.enter3 !== "null") {
      console.log(this.state.enter3)
      penerima.push(this.state.enter3)
    }

    if (this.state.enter4 !== null || this.state.enter4 !== "null") {
      console.log(this.state.enter4)
      penerima.push(this.state.enter4)
    }

    console.log(penerima) 

    var penerimafix = penerima.filter(function (el) {
      return el !== "";
    });
    
    console.log(penerimafix);

    const data = await {
      Tanggal: this.state.tgl,
      Jenis: this.state.jenis === "Lain" ? this.state.other : this.state.jenis,
      Via: this.state.pengeluaran,
      No_Trx: this.state.advance,
      Divisi: this.state.users.divisi_name,
      Project_name: this.state.projectname,
      Penerima: penerima.length > 1 ? penerimafix.join() : this.state.enter0,
      PIC: this.state.detailEntertaint.created_by,
      Perusahaan: this.state.namaperusahaan,
      Nominal: this.state.nominal,
      Periode: this.state.cc,
      SelectedID: selectedIdEntertaint

    }

    console.log(data)
    const url = "api/v1/entertaint/updateentertaint"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));


    if (this.state.response.status === 'success') {
      this.setState({open:true})
      this.setState ({
        tgl: null,
        jenis: null,
        users: null,
        pengeluaran: null,
        divisi: null,
        advance: null,
        cc: "",
        projectname: null,
        penerima: null,
        namaperusahaan: null,
        nominal: null,
        response:null,
        detailEntertaint: null,
        selectedIdEntertaint : null
      })
      window.location.reload()

    }
  }

  handleClose = (event, reason) => {
    this.setState({ 
      tgl: null,
      users: null,
      jenis: null,
      other: null,
      pengeluaran: null,
      advance: null,
      projectname: null,
      picpenerima: null,
      penerima: null,
      enter0: null,
      enter1: null,
      enter2: null,
      enter3: null,
      enter4: null,
      namaperusahaan: null,
      vialabel: "",
      nominal: null,
    });
    if (reason === 'clickaway') {
      this.setState({ 
        tgl: null,
        users: null,
        jenis: null,
        other: null,
        pengeluaran: null,
        advance: null,
        projectname: null,
        picpenerima: null,
        penerima: null,
        enter0: null,
        enter1: null,
        enter2: null,
        enter3: null,
        enter4: null,
        namaperusahaan: null,
        vialabel: "",
        nominal: null,
      });
      return;
    }
    this.setState({ open: false,
     
    });
  };

  toogleShow() {
    this.setState({ hidden: !this.state.hidden })
  }

  render() {
    const { isDialogOpen, handleDialogClose } = this.props
    return (
      <Dialog open={isDialogOpen} onClose={handleDialogClose} fullWidth={true} maxWidth="md">
        <DialogTitle onClose={handleDialogClose}>Edit Entertaint</DialogTitle>
        <DialogContent>
          <AvForm onValidSubmit={e => this.handleFormSubmit(e)} >
            <AvGroup style={{ position: 'relative', top: '0rem' }}>
              <Label className="avenir-black-primary" for="tgl">
                Tanggal Entertaint (mm/dd/yyyy)
              </Label>
              <AvInput value={this.state.tgl} type="date" name="tgl" id="tgl" placeholder="Insert Tanggal" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
              <AvFeedback>Input Tanggal Entertaint</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '1rem' }}>
              <Label className="avenir-black-primary" for="jenis">
                Jenis Entertaint
              </Label>
              <AvInput value={this.state.jenis} type="select" name="jenis" id="jenis" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }}>
                <option value="" >Select Jenis Entertaint</option>
                <option value="Jamuan" >Jamuan Makan</option>
                <option value="Golf" >Golf</option>
                <option value="Karaoke" >Karaoke</option>
                <option value="Gift" >Gift</option>
                <option value="Lain" >Lainnya</option>
              </AvInput>
              <AvFeedback>Select Jenis Entertaint required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '2rem', display: `${this.state.jenis === "Lain" ? "" : "none"}` }}>
              <Label className="avenir-black-primary" for="other">
                Lainnya
              </Label>
              <AvInput enable value={this.state.other} type="text" name="other" id="other" placeholder="Insert Lainnya Jenis Entertaint" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '3rem' }}>
              <Label className="avenir-black-primary" for="pengeluaran">
                Pengeluaran Via
              </Label>
              <AvInput value={this.state.pengeluaran} type="select" name="pengeluaran" id="pengeluaran" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }}>
                <option value="" >Select Pengeluaran Via</option>
                <option value="Advance Settlement" >Advance Settlement </option>
                <option value="Reimbursement" >Reimbursement</option>
                <option value="Corporate Credit Card" >Corporate Credit Card</option>
              </AvInput>
              <AvFeedback>Select Pengeluaran required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '4rem', display: `${this.state.hidden1}` }}>
              <Label className="avenir-black-primary" for="advance">
                {this.state.vialabel}
              </Label>
              <AvInput required value={this.state.advance} onChange={e => this.handleenter1Change(e)} type="text" name="advance" id="advance" placeholder="Input nomor AS / Reimburst" style={{ width: "30%" }} />
              <AvFeedback>Input No required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '4rem' }}>
              <Label className="avenir-black-primary" for="advance">
                Periode Corporate CC
              </Label>
              <AvInput value={this.state.cc} required={this.state.isRequired} onChange={e => this.handleenter1Change(e)} type="text" name="cc" id="cc" placeholder="MM/YY" style={{ width: "30%" }} />
              <AvFeedback>Input Periode required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '5rem' }}>
              <Label className="avenir-black-primary" for="projectname">
                Nama Proyek
              </Label>
              <AvInput value={this.state.projectname} type="text" onChange={e => this.handleenter1Change(e)} name="projectname" id="projectname" placeholder="Input Nama Proyek" style={{ width: "30%" }} />
              <AvFeedback>Input Nama Proyek required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '6rem' }}>
              <Label className="avenir-black-primary" for="enter0">
                Nama Penerima Entertaint #1
              </Label>
              <AvInput required value={this.state.enter0} type="text" name="enter0" id="enter0" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
              <AvFeedback>Input Nama Penerima required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '7rem' }}>
              <Label className="avenir-black-primary" for="enter2">
                Nama Penerima Entertaint #2
              </Label>
              <AvInput enable value={this.state.enter1} type="text" name="enter1" id="enter2" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '8rem' }}>
              <Label className="avenir-black-primary" for="enter3">
                Nama Penerima Entertaint #3
              </Label>
              <AvInput enable value={this.state.enter2} type="text" name="enter2" id="enter3" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '9rem' }}>
              <Label className="avenir-black-primary" for="enter4">
                Nama Penerima Entertaint #4
              </Label>
              <AvInput enable value={this.state.enter3} type="text" name="enter3" id="enter4" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '10rem' }}>
              <Label className="avenir-black-primary" for="enter5">
                Nama Penerima Entertaint #5
              </Label>
              <AvInput value={this.state.enter4} type="text" name="enter4" id="enter5" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />

            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '11rem' }}>
              <Label className="avenir-black-primary" for="namaperusahaan">
                Nama Perusahaan / Instansi Penerima Entertaint
              </Label>
              <AvInput value={this.state.namaperusahaan} type="text" name="namaperusahaan" id="namaperusahaan" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
              <AvFeedback>Input Nama Perusahaan required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '12rem' }}>
              <Label className="avenir-black-primary" for="nominal">
                Nominal Entertaint
              </Label>
              <AvInput value={this.state.nominal} type="number" name="nominal" id="nominal" placeholder="Insert Nominal" onChange={e => this.handleenter1Change(e)} style={{ width: "30%" }} />
              <AvFeedback>Input Nominal required</AvFeedback>
            </AvGroup>

            <AvGroup style={{ position: 'relative', top: '13rem' }}>
              <Button
                type="submit"
                name="tutup"
                onClick={e => this.setSubmit(e, "tutup")}
                variant="outlined"
                color="primary"
                className="px-10"
                style={{ fontSize: 14 }}>
                <h9 style={{ color: "#007bff", fontWeight: 'bold' }}>Simpan & Tutup</h9>
              </Button>
            </AvGroup>
          </AvForm>
        </DialogContent>
      </Dialog>
    )
  }
}
