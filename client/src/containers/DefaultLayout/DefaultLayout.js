import React, { Component, Suspense } from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import { Container } from "reactstrap"
import CircularProgress from "@material-ui/core/CircularProgress"

import {
  AppHeader,
  AppSidebar,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from "@coreui/react"
// sidebar nav config
import navigation from "../../_nav"
import navigation_1 from "../../_nav1"
// routes config
import routes from "../../routes"

// const DefaultAside = React.lazy(() => import("./DefaultAside"))
// const DefaultFooter = React.lazy(() => import("./DefaultFooter"))
const DefaultHeader = React.lazy(() => import("./DefaultHeader"))

class DefaultLayout extends Component {

  state={
    user : null
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

 async signOut(e) {
    e.preventDefault()
    await localStorage.removeItem('token');
    await localStorage.removeItem('user');
    this.props.history.push("/login")
  }

  async componentDidMount() {
    if (!localStorage.getItem('user')){
      await localStorage.removeItem('token');
      await localStorage.removeItem('user');
      return this.props.history.push("/login");
     
    }else{
    var users = await JSON.parse(localStorage.getItem("user"))
    await this.setState({user : users});
    console.log("ini hasil login", this.state.user)
    }
    
  }

  generateNav = () => {
    if(this.state.user.role === 1 ){
      return navigation
    }else{
      return navigation_1
    }
  }

  render() {
    // Configuring names for each main rendered component
    const route = routes.find(loc => loc.path === this.props.location.pathname)
    const heading = route.name

    return (
      <>
      {this.state.user ? (<>
        <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader user={this.state.user} onLogout={e => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarMinimizer />
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={this.generateNav()} {...this.props} />
            </Suspense>
          </AppSidebar>
          <main className="main">
            <h2
              className="avenir-black-primary"
              style={{
                marginTop: 20,
                marginBottom: 20,
                marginLeft: 30,
                display: "inline-block"
              }}>
              {heading}
            </h2>
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => <route.component {...props} />}
                      />
                    ) : null
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </Container>
          </main>
        </div>
      </div>
      
      </>) : (
         <CircularProgress style={{ marginLeft: 10, color: "#007bff" }} />
      )}
      
      </>
      
    ) 
  }
}

export default DefaultLayout
