import React, { Component } from "react"
import { Link, NavLink, withRouter } from "react-router-dom"
import {
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap"
import PropTypes from "prop-types"
import {
  isMobile,
  BrowserView,
  MobileView
} from "react-device-detect";
import {
  AppSidebarToggler
} from "@coreui/react"
import logo from "../../assets/img/brand/logo.png"
import { withStyles } from "@material-ui/core/styles"

const propTypes = {
  children: PropTypes.node
}

const defaultProps = {}

const styles = theme => ({
  avatar: {
    margin: 10,
    width: 30,
    height: 30
  }
})

class DefaultHeader extends Component {

  handleMyAccountClick = () => {
    // this.props.history.push("/myaccount")
    console.log(this.props)
    // Redirect()
  }

  renderNavbar = () => {

    return (
      <div>
        <BrowserView>
          <Nav className="ml-auto" navbar>
            <NavItem style={{ marginRight: '4rem', marginTop: 10 }}>
              <NavLink className="nav-link1" activeClassName="actives" to="/dashboard">
                <h6 className="avenir-black-primary navbar-item">Dashboard</h6>
              </NavLink>
            </NavItem>
            <NavItem style={{ marginRight: '4rem', marginTop: 10 }}>
              <NavLink className="nav-link1" activeClassName="actives" to="/myaccount">
                <h6 className="avenir-black-primary navbar-item">My Account</h6>
              </NavLink>
            </NavItem>

            <NavItem style={{ marginRight: '4rem', marginTop: 10 }}>
              <Link className="nav-link1" activeClassName="actives" to="">
                <h6 className="avenir-black-primary navbar-item" onClick={e => this.props.onLogout(e)}>Sign Out</h6>
              </Link>
            </NavItem>
          </Nav>
        </BrowserView>

        <MobileView>
          <Nav className="ml-auto" navbar>
            <UncontrolledDropdown nav>
              <DropdownToggle nav>
                <i className="fas fa-ellipsis-v"></i>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem style={{marginBottom:'0px !important'}} onClick={e => this.props.history.push('/dashboard')}>
                  Dashboard
              </DropdownItem>
                <DropdownItem onClick={e => this.props.history.push('/myaccount')}>
                  My Account
              </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={e => this.props.onLogout(e)}>
                  Logout
              </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </MobileView>
      </div>
    )
  }

  render() {
    // eslint-disable-next-line
    const { user, classes, children, ...attributes } = this.props

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-md-none" display="md" mobile />
        <img
          src={logo}
          alt="Immobi Tracker Logo"
          style={{ width: 210, height: 35, marginLeft: 10 }}
        />
        {this.renderNavbar()}
      </React.Fragment>
    )
  }
}

DefaultHeader.propTypes = propTypes
DefaultHeader.defaultProps = defaultProps

export default withRouter(withStyles(styles)(DefaultHeader))
