import React, { Component } from "react"
import { Row, Col, Label, Input, FormGroup, Button } from "reactstrap"
import { CircularProgress } from "@material-ui/core"

import "./MyAccount.scss"

export default class MyAccount extends Component {

  constructor(props) {
    super(props);
    //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
    this.state = {
      user: null,
      name: null,
      password: null,
      email: null,
      isEditing: false,
      isLoading:false
    };
  }

  async componentDidMount() {
    var users = await JSON.parse(localStorage.getItem("user"));
    await fetch("api/v1/user/myaccount/" + users.id)
      .then(resp => {
        console.log(resp.status, resp.statusText)
        return resp.json()
      })
      .then(users => {
        this.setState({ user: users[0][0] })
      })
      .catch(err => console.log(err))

    await this.setState({ name: this.state.user.Name, role: this.state.user.Role, email: this.state.user.Email })
    console.log("ini hasil login", this.state.user)
  }

  handleClickEdit = async (e) => {
    this.setState({ isEditing: !this.state.isEditing })
  }

  handleClickSave = async (e) => {
    this.setState({isLoading: !this.state.isLoading})
    e.preventDefault();
    var form = {
      userId: parseInt(this.state.user.ID),
      Name: this.state.name,
      Email: this.state.email,
      Password: this.state.password
    }

    const url = "api/v1/user/myaccount/update"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(form),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));

    if (this.state.response.status === 'success') {

      this.setState({
        name: '',
        password: '',
        email: '',
      })
      window.location.reload();
      this.setState({ isEditing: false })
    }

    console.log(form)

  }

  handleChange = (e) => {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    })
  }

  generateBody = () => {
    if (this.state.isEditing === false) {
      return (
        <React.Fragment>
          <Row>
            <Col lg="2">
            </Col>
            <Col lg="4">
              <FormGroup>
                <Label className="avenir-black-primary" for="fullname">
                  Name
              </Label>
                <Input disabled value={this.state.name} type="text" name="name" id="name" placeholder="Input Full Name" />
              </FormGroup>
            </Col>
            <Col lg="4">
              <FormGroup>
                <Label className="avenir-black-primary" for="email">
                  Email
              </Label>
                <Input disabled value={this.state.email} type="email" name="email" id="email" placeholder="Input email" />
              </FormGroup>

              <Button
                variant="outlined"
                color="primary"
                className="px-4"
                onClick={e => this.handleClickEdit(e)}
                style={{ fontSize: 18, position: "absolute", right: 0 }}>
                Edit
            </Button>
            </Col>
            <Col lg="2" />
          </Row>
        </React.Fragment>
      )

    } else {
      return (
        <React.Fragment>
          <Row>
            <Col lg="2">
            </Col>
            <Col lg="4">
              <FormGroup>
                <Label className="avenir-black-primary" for="name">
                  Fullname
              </Label>
                <Input value={this.state.name} onChange={e => this.handleChange(e)} type="text" name="name" id="name" placeholder="Input Full Name" />
              </FormGroup>
            </Col>
            <Col lg="4">
              <FormGroup>
                <Label className="avenir-black-primary" for="email">
                  Email
              </Label>
                <Input value={this.state.email} onChange={e => this.handleChange(e)} type="email" name="email" id="email" placeholder="Input email" />
              </FormGroup>
              <FormGroup style={{ marginTop: "20%" }}>
                <Label className="avenir-black-primary" for="password">
                  New Password
              </Label>
                <Input value={this.state.password} onChange={e => this.handleChange(e)} type="password" name="password" id="password" placeholder="Input new password" />
              </FormGroup>
              <Button
                color="primary"
                className="px-4"
                onClick={e => this.handleClickSave(e)}
                style={{ fontSize: 18, position: "absolute", right: 0 }}>
                {this.state.isLoading ? <CircularProgress size={25} color={"white"}/> : "Save"}
            </Button>
              <Button
                color="primary"
                className="px-4"
                onClick={e => this.handleClickEdit(e)}
                style={{ fontSize: 18, position: "absolute", right: '30%' }}>
                Cancel
            </Button>
            </Col>
            <Col lg="2" />
          </Row>
        </React.Fragment>
      )
    }

  }

  render() {
    return (
      this.generateBody()
    )
  }
}
