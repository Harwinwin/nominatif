import React, { Component } from "react"
import { Label } from "reactstrap"
import "./NewEntertaint.scss"
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import Button from '@material-ui/core/Button'
import { withRouter } from 'react-router-dom';

import './NewEntertaint.scss'

class NewEntertaint extends Component {
  constructor(props) {
    super(props);
    //usahain buat penampung variablenya dlu di state kalo beda folder ya pake props
    this.state = {
      tgl: null,
      users: null,
      jenis: null,
      other: null,
      pengeluaran: null,
      advance: null,
      projectname: null,
      picpenerima: null,
      enter0: null,
      enter2: null,
      enter3: null,
      enter4: null,
      enter5: null,
      namaperusahaan: null,
      vialabel: "",
      nominal: null,
      hidden: "none",
      hidden1: "none",
      submitName: "",
      cc: "",
      isRequired: false
    };
  }

  async handleenter1Change(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if (name === "jenis") {
      if (target.value === "Lain") {
        this.setState({ hidden: "" })
      }
      else {
        this.setState({ hidden: "none" })
      }
    } else if (name === "pengeluaran") {
      console.log(target.value)
      if (target.value === "") {
        this.setState({ hidden1: "none" })
      } else if (target.value === "Corporate Credit Card") {
        await this.setState({ hidden1: "", vialabel: target.value, isRequired: 'enable' })
        console.log(this.state.isRequired)
      } else {
        this.setState({ hidden1: "", vialabel: target.value, isRequired: false })
      }
    }

    this.setState({
      [name]: value
    })
  }

  setSubmit = (e, value) => {
    this.setState({ submitName: value })
  }

  handleFormSubmit = async (e, value) => {
    e.preventDefault()
    var penerima = []

    if (this.state.enter0 !== null) {
      console.log("enter0")
      penerima.push(this.state.enter0)
    }

    if (this.state.enter2 !== null) {
      console.log("enter2")
      penerima.push(this.state.enter2)
    }

    if (this.state.enter3 !== null) {
      console.log("enter3")
      penerima.push(this.state.enter3)
    }

    if (this.state.enter4 !== null) {
      console.log("enter4")
      penerima.push(this.state.enter4)
    }

    if (this.state.enter5 !== null) {
      console.log("enter5")
      penerima.push(this.state.enter5)
    }

    const data = await {
      Tanggal: this.state.tgl,
      Jenis: this.state.jenis === "Lain" ? this.state.other : this.state.jenis,
      Via: this.state.pengeluaran,
      No_Trx: this.state.advance,
      Divisi: this.state.users.divisi_name,
      Project_name: this.state.projectname,
      Penerima: penerima.length > 1 ? penerima.join() : this.state.enter0,
      PIC: this.state.picpenerima,
      PIC_name: this.state.users.Name,
      status: 1,
      Perusahaan: this.state.namaperusahaan,
      Nominal: this.state.nominal,
      Periode: this.state.cc

    }
    console.log(data)
    const url = "api/v1/entertaint/newentertaint"

    await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(res => {
        return res.json()
      })
      .then(response => {
        this.setState({ response })
      })
      .catch(error => console.error('Error:', error));


    if (this.state.response.status === 'success') {
      if (this.state.submitName === "tutup") {
        this.props.history.push("/rekapentertaint");
      } else if (this.state.submitName === "new") {
        window.location.reload();
      } else if (this.state.submitName === "print") {
        this.props.history.push({
          pathname: '/exportentertaint',
          state: { printData: data }
        })
      }
    }
  }


  toogleShow() {
    this.setState({ hidden: !this.state.hidden })
  }

  async componentDidMount() {
    var users = await JSON.parse(localStorage.getItem("user"));
    this.setState({ picpenerima: users.id })
    await fetch("api/v1/user/myaccount/" + users.id)
      .then(resp => {
        console.log(resp.status, resp.statusText)
        return resp.json()
      })
      .then(users => {

        this.setState({ users: users[0][0] })
      })
      .catch(err => console.log(err))

    console.log(this.state.users);
    if (this.props.enter1) {
      this.setState({ enter1: this.props.enter1 });
    }
  }

  render() {
    return (
      <div>
        <AvForm onValidSubmit={e => this.handleFormSubmit(e)} >
          <AvGroup>
            <Label className="avenir-black-primary" for="tgl">
              Tanggal Entertaint
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.tgl} type="date" name="tgl" id="tgl" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
            <AvFeedback>Input Tanggal Entertaint</AvFeedback>
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="jenis">
              Jenis Entertaint
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.jenis} type="select" name="jenis" id="jenis" onChange={e => this.handleenter1Change(e)}>
              <option value="" >Select Jenis Entertaint</option>
              <option value="Jamuan" >Jamuan Makan</option>
              <option value="Golf" >Golf</option>
              <option value="Karaoke" >Karaoke</option>
              <option value="Gift" >Gift</option>
              <option value="Lain" >Lainnya</option>
            </AvInput>
            <AvFeedback>Select Jenis Entertaint required</AvFeedback>
          </AvGroup>
          <AvGroup style={{ display: `${this.state.hidden}` }}>
            <Label className="avenir-black-primary" for="other">
              Lainnya
              </Label>
            <AvInput className = "input-form-entertaint" enable value={this.state.other} type="text" name="other" id="other" placeholder="Insert Lainnya Jenis Entertaint" onChange={e => this.handleenter1Change(e)} />
          </AvGroup>
          <AvGroup>
            <Label className="avenir-black-primary" for="pengeluaran">
              Pengeluaran Via
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.pengeluaran} type="select" name="pengeluaran" id="pengeluaran" onChange={e => this.handleenter1Change(e)}>
              <option value="" >Select Pengeluaran Via</option>
              <option value="Advance Settlement" >Advance Settlement </option>
              <option value="Reimbursement" >Reimbursement</option>
              <option value="Corporate Credit Card" >Corporate Credit Card</option>
            </AvInput>
            <AvFeedback>Select Pengeluaran required</AvFeedback>
          </AvGroup>

          <AvGroup style={{ display: `${this.state.hidden1}` }}>
            <Label className="avenir-black-primary" for="advance">
              {this.state.vialabel}
            </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.advance} onChange={e => this.handleenter1Change(e)} type="text" name="advance" id="advance" placeholder="Input nomor AS / Reimburst" />
            <AvFeedback>Input No required</AvFeedback>
          </AvGroup>

          <AvGroup style={{ display: `${this.state.hidden1}` }}>
            <Label className="avenir-black-primary" for="advance">
              Periode Corporate CC
              </Label>
            <AvInput className = "input-form-entertaint" value={this.state.cc} required={this.state.isRequired} onChange={e => this.handleenter1Change(e)} type="text" name="cc" id="cc" placeholder="MM/YY" />
            <AvFeedback>Input Periode required</AvFeedback>
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="projectname">
              Nama Proyek
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.projectname} type="text" onChange={e => this.handleenter1Change(e)} name="projectname" id="projectname" placeholder="Input Nama Proyek" />
            <AvFeedback>Input Nama Proyek required</AvFeedback>
          </AvGroup>

          <AvGroup >
            <Label className="avenir-black-primary" for="enter0">
              Nama Penerima Entertaint #1
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.enter0} type="text" name="enter0" id="enter0" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
            <AvFeedback>Input Nama Penerima required</AvFeedback>
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="enter2">
              Nama Penerima Entertaint #2
              </Label>
            <AvInput className = "input-form-entertaint" enable value={this.state.enter2} type="text" name="enter2" id="enter2" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="enter3">
              Nama Penerima Entertaint #3
              </Label>
            <AvInput className = "input-form-entertaint" enable value={this.state.enter3} type="text" name="enter3" id="enter3" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="enter4">
              Nama Penerima Entertaint #4
              </Label>
            <AvInput className = "input-form-entertaint" enable value={this.state.enter4} type="text" name="enter4" id="enter4" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="enter5">
              Nama Penerima Entertaint #5
              </Label>
            <AvInput className = "input-form-entertaint" value={this.state.enter5} type="text" name="enter5" id="enter5" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="namaperusahaan">
              Nama Perusahaan / Instansi Penerima Entertaint
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.namaperusahaan} type="text" name="namaperusahaan" id="namaperusahaan" placeholder="Insert Nama Penerima" onChange={e => this.handleenter1Change(e)} />
            <AvFeedback>Input Nama Perusahaan required</AvFeedback>
          </AvGroup>

          <AvGroup>
            <Label className="avenir-black-primary" for="nominal">
              Nominal Entertaint
              </Label>
            <AvInput className = "input-form-entertaint" required value={this.state.nominal} type="number" name="nominal" id="nominal" placeholder="Insert Nominal" onChange={e => this.handleenter1Change(e)} />
            <AvFeedback>Input Nominal required</AvFeedback>
          </AvGroup>

          <AvGroup >
            <Button
              type="submit"
              name="tutup"
              onClick={e => this.setSubmit(e, "tutup")}
              variant="outlined"
              color="primary"
              className="btn-closed">
              <h9 style={{ color: "#007bff", fontWeight: 'bold' }}>Simpan & Tutup</h9>
            </Button>

            <Button
              type="submit"
              onClick={e => this.setSubmit(e, "new")}
              name="new"
              variant="outlined"
              color="primary"
              className="btn-reload-new">
              <h9 style={{ color: "#007bff", fontWeight: 'bold' }}>Simpan & Rekam Baru</h9>
            </Button>

            <Button
              type="submit"
              onClick={e => this.setSubmit(e, "print")}
              variant="contained"
              color="primary"
              className="btn-print-new">
              <h9 style={{ fontWeight: 'bold' }}>Cetak Nominatif Entertaint</h9>
            </Button>
          </AvGroup>
        </AvForm>

      </div>
    )
  }
}

export default withRouter(NewEntertaint)