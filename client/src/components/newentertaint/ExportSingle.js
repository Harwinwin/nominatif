import React from "react"
import TablePrint from './TablePrint'

export default class PrintTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            rekapSingle: null,
            users: null,
            atasan: ""
        }
    }

    async componentDidMount() {
        var users = await JSON.parse(localStorage.getItem("user"))
        const { printData } = await this.props.location.state
        this.setState({rekapSingle : printData})
        //fetch utk last datanya dan users yg sedang login
        await Promise.all([fetch(`api/v1/user/viewbyid/${users.id}`)])
            .then(([usersResp]) => {
                return Promise.all([usersResp.json()])
            })
            .then(([users]) => {
                console.log(users[0])
                this.setState({users: users[0][0] })
            })
            .catch(error => console.log(error))
            console.log(this.state.users)
            
        if (this.state.users.Role === 2 ) {
            await fetch(`api/v1/user/getAtasan/${users.divisi}`)
                .then(resp => {
                    return resp.json()
                })
                .then(atasan => {

                    this.setState({ atasan })
                })
                .catch(err => console.log(err))
        }

    }

    render() {

        return (
            <>
                {this.state.rekapSingle && this.state.users? (
                    <>
                        <TablePrint printData={this.state.rekapSingle} User={this.state.users} atasan={this.state.atasan}/>
                    </>
                ) :

                    <div>

                    </div>
                }
            </>


        )
    }


}



