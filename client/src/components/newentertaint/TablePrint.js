import React from "react"
import { withStyles } from "@material-ui/core/styles"
import { withRouter } from 'react-router-dom';
import ReactToPrint from "react-to-print";

import { Container, Row, Col, Button } from 'reactstrap';

const styles = theme => ({
    avatar: {
        margin: 10,
        width: 30,
        height: 30
    },
    iconBluish: {
        color: "#007bff"
    },
    iconGreenCircle: {
        color: "#07e364",
        marginRight: 5
    },
    // iconTrash: {
    //   fontSize: 15
    // },
    iconEdit: {
        marginRight: 10,
        fontSize: 15
    },
    tableCell: {
        padding: 10,
        fontSize: 12,
        whiteSpace: 'nowrap',
        border: '1px solid'
    },
    tableCellTight: {
        width: 70,
        paddingRight: 5,
        paddingLeft: 5
    },
    tableCellHeader: {
        padding: 5,
        fontSize: 12,
        color: 'black',
        border: '1px solid',
        fontWeight: '1000',
        textAlign: 'center'
    }
})



class TablePrint extends React.PureComponent {

    generateSignature = () => {
        if (this.props.User.Role === 2) {
            return (
                <div>
                    <Row style={{ marginTop: '10%', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'} >{this.props.User.Name}<div style={{ width: '200px', borderBottom: '1px solid' }}></div></Col>
                        <Col align={'center'} >{this.props.atasan.user_name}<div style={{ width: '200px', borderBottom: '1px solid', position: 'relative' }}></div></Col>
                        <Col align={'center'} ><div style={{ width: '200px', borderBottom: '1px solid', top: '1rem', position: 'relative' }}></div></Col>
                    </Row>
                    <Row style={{ marginTop: '0', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'}>PIC Entertaint</Col>
                        <Col align={'center'}>Manager/Head of Division</Col>
                        <Col align={'center'}>Director</Col>
                    </Row>

                </div>
            )
        } else if (this.props.User.Role === 3) {
            return (
                <div>
                    <Row style={{ marginTop: '10%', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'}>{this.props.User.Name}<div style={{ width: '200px', borderBottom: '1px solid' }}></div></Col>
                        <Col align={'center'}><div style={{ width: '200px', borderBottom: '1px solid', position: 'relative', top: '1rem' }}></div></Col>
                        <Col align={'center'}><div style={{ width: '200px', borderBottom: '1px solid', position: 'relative', top: '1rem' }}></div></Col>

                    </Row>
                    <Row style={{ marginTop: '0', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'}>PIC Entertaint</Col>
                        <Col align={'center'}>Manager/Head Of Division</Col>
                        <Col align={'center'}>Director</Col>
                    </Row>
                </div>

            )
        } else if (this.props.User.Role === 1) {
            return (
                <div>
                    <Row style={{ marginTop: '10%', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'}><div style={{ width: '200px', borderBottom: '1px solid' }}></div></Col>
                        <Col align={'center'}><div style={{ width: '200px', borderBottom: '1px solid' }}></div></Col>
                        <Col align={'center'}><div style={{ width: '200px', borderBottom: '1px solid' }}></div></Col>
                    </Row>
                    <Row style={{ marginTop: '0', fontSize: 12, fontWeight: 1000 }}>
                        <Col align={'center'}>PIC Entertaint</Col>
                        <Col align={'center'}>Manager/Head Of Division</Col>
                        <Col align={'center'}>Director</Col>
                    </Row>
                </div>

            )
        }
    }


    render() {
        var formatter = new Intl.NumberFormat('id', {
            style: 'currency',
            currency: 'IDR',
        });

        var rp = formatter.format(this.props.printData.Nominal)
        const { classes } = this.props
        const { printData } = this.props
        return (
            <>
                {printData ? (
                    <>
                        <div>

                            <div pageStyle="@page { size: A4 landscape;}" size="small" ref={el => (this.componentRef = el)} style={{ width: '100%', padding: 25 }}>
                                <h5>PT IMMOBI SOLUSI PRIMA</h5>
                                <h5 style={{ marginBottom: '10%' }}>Daftar Nominatif Entertaint</h5>
                                <table >
                                    <thead className={classes.tableHead}>

                                        <th className={classes.tableCellHeader}>No</th>
                                        <th align="center" className={classes.tableCellHeader}>Tanggal Entertaint</th>
                                        <th align="center" className={classes.tableCellHeader}>Jenis Entertaint</th>
                                        <th align="center" className={classes.tableCellHeader}>PIC Entertaint</th>
                                        <th align="center" className={classes.tableCellHeader}>Divisi</th>
                                        <th align="center" className={classes.tableCellHeader}>Pengeluaran via</th>
                                        <th align="center" className={classes.tableCellHeader}>No. Reimbursement/Advance Settlement </th>
                                        <th align="center" className={classes.tableCellHeader}>Periode Corporate CC </th>
                                        <th align="center" className={classes.tableCellHeader}>Nama Proyek </th>
                                        <th align="center" className={classes.tableCellHeader}>Nama Penerima Entertaint </th>
                                        <th align="center" className={classes.tableCellHeader}>Nama Penerima Perusahaan Entertaint </th>
                                        <th align="center" className={classes.tableCellHeader}>Nominal (Rp.) </th>

                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className={classes.tableCell}>1</td>
                                            <td className={classes.tableCell}>{printData.Tanggal}</td>
                                            <td className={classes.tableCell}>{printData.Jenis}</td>
                                            <td className={classes.tableCell}>{printData.PIC_name}</td>
                                            <td className={classes.tableCell}>{printData.Divisi}</td>
                                            <td className={classes.tableCell}>{printData.Via}</td>
                                            <td className={classes.tableCell}>{printData.No_Trx}</td>
                                            <td className={classes.tableCell}>{printData.Periode === "null" ? "" : printData.Periode}</td>
                                            <td className={classes.tableCell}>{printData.Project_name}</td>
                                            <td className={classes.tableCell}>{printData.Penerima}</td>
                                            <td className={classes.tableCell}>{printData.Perusahaan}</td>
                                            <td className={classes.tableCell}>{rp}</td>
                                        </tr>
                                        <tr>
                                            <td style={{ fontWeight: 1000 }} className={classes.tableCell} align={"center"} colSpan={'11'}>Grand Total</td>
                                            <td style={{ fontWeight: 1000 }} className={classes.tableCell}>{rp}</td>
                                        </tr>

                                    </tbody>
                                </table>

                                <Container style={{ marginTop: '10%' }}>
                                    <Row style={{ fontSize: 12, fontWeight: 1000 }}>
                                        <Col align={'center'}>Created By</Col>
                                        <Col align={'center'}>Acknowledged By</Col>
                                        <Col align={'center'}>Approved By</Col>
                                    </Row>

                                    {this.generateSignature()}



                                </Container>


                            </div>

                            <ReactToPrint
                                trigger={() => <Button style={{ marginLeft: '75%', width: '15%', fontSize: '20px' }} color="primary" className="btn-add-new" size="sm" >
                                    Cetak
                                                </Button>}
                                content={() => this.componentRef}
                                pageStyle="@page { size: A4 landscape;}"
                            />

                        </div>
                    </>
                ) : (
                        <h1>Tidak ada data</h1>
                    )}

            </>

        )
    }


}


export default withRouter(withStyles(styles)(TablePrint))
