import React from "react";
import TablePrint from './PrintAllTable';


export default class ExportAll extends React.Component {
    constructor(props){
        super(props);

        this.state={
            rekapAll : null,
            users : null,
            atasan : ""
        }
    }

    async componentDidMount(){
        var printdata = await JSON.parse(localStorage.getItem("exportdata"));
        await this.setState({ rekapAll: printdata[0] })
        var users = await JSON.parse(localStorage.getItem("user"))
        
        //fetch utk last datanya dan users yg sedang login
        await Promise.all([fetch(`api/v1/user/viewbyid/${users.id}`)])
            .then(([usersResp]) => {
                return Promise.all([usersResp.json()])
            })
            .then(([users]) => {
                this.setState({ users: users[0][0] })
            })
            .catch(error => console.log(error))
            console.log(this.state.users)
            
        if (this.state.users.Role === 2 ) {
            await fetch(`api/v1/user/getAtasan/${this.state.users.divisi}`)
                .then(resp => {
                    return resp.json()
                })
                .then(atasan => {

                    this.setState({ atasan })
                })
                .catch(err => console.log(err))
        }

        console.log(this.state.rekapAll)
        console.log(this.state.atasan)

    }

    

    render() {

        return (
            <>
                {this.state.rekapAll && this.state.users?(
                    <>
                         <TablePrint printData = {this.state.rekapAll} User={this.state.users} atasan={this.state.atasan}/>
                    </>
                ) :
                
                    <div>

                    </div>
                }
            </>
                    
          
        )
    }


}



