"use strict"

const Database = use("Database")
const Helpers = use('Helpers')

const Route = use("Route")

const api = "api/v1"

Route.group(() => {

  Route.post("/register", "AuthController.register")
  Route.post("/login", "AuthController.login")
})
  .prefix(api)
  .formats(["json"])

//! Dashboard
Route.group(() => {
  Route.get("/entertaint/getdatabymonth/:year", "DashboardController.getDataByMonth")
  Route.get("/entertaint/getdatabystatus/:year", "DashboardController.getDataByStatus")
})
  .prefix(api)
  .formats(["json"])

//! Entertain
Route.group(() => {
  Route.post("/entertaint/newentertaint", "EntertainController.addEntertain")
  Route.get("/entertaint/getentertaintbyid/:id", "EntertainController.getEntertaintById")
  Route.post("/entertaint/updateentertaint", "EntertainController.updateEntertaint")
  Route.get("/entertaint/getlastid", "EntertainController.getLatestEntertain")
  Route.get("/entertaint/getdata/:userId/:roleId/:divisiId", "EntertainController.getAllEntertain")
  Route.post("/entertaint/changestatus", "EntertainController.changeStatus")
  Route.post("/entertaint/deletedentertaint", "EntertainController.deleteEntertaint")
})
  .prefix(api)
  .formats(["json"])

//!Users
Route.group(() => {
  Route.get("user/userviewall", "UserController.getuserViewAll")
  Route.post("user/addnewuser", "UserController.postaddNewuser")
  Route.get("user/myaccount/:id", "UserController.getuserbyID")
  Route.post("user/myaccount/update", "UserController.updateProfileByID")
  Route.post("user/edituser", "UserController.updateUserByID")
  Route.get("user/viewbyid/:id", "UserController.getUserDetail")
  Route.get("user/getData", "UserController.getDataInput")
  Route.get("user/getAtasan/:divisi", "UserController.getUserAtasan")
  Route.post("/user/deleteduser", "UserController.deleteUser")
})
  .prefix(api)
  .formats(["json"])

