'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParamSchema extends Schema {
  up () {
    this.create('params', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('params')
  }
}

module.exports = ParamSchema
