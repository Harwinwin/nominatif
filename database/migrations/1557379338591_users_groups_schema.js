'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersGroupsSchema extends Schema {
  up () {
    this.table('users_groups', (table) => {
      // alter table
    })
  }

  down () {
    this.table('users_groups', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UsersGroupsSchema
