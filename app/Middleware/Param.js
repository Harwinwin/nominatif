'use strict'

const debug = require('debug')('middleware:param')

class Param {
  
  async handle ({ request, response, params, view }, next) {

      debug('handle')
  
      this.handleUpStream ({ request, response, params, view })
      
      await next()
  
      //this.handleDownStream ({ request, response, params, view })
  }

  handleUpStream ({ request, response, params, view }) {
    debug('handleUpStream')

    const reqParams = request.all()

    // workaround
    if (reqParams.tech && !Array.isArray(reqParams.tech)) reqParams.tech = reqParams.tech.split(',')

    const paramChecks = {
      API : ['a', 'api'],
      assignedTo : ['at', 'assigned_to'],
      BRD : ['b', 'brd'],
      BAST : ['ba', 'bast'],
      backEnd : ['be', 'back_end'],
      businessAnalyst : ['ba', 'business_analyst', 'businessAnalyst'],
      createdBy : ['cb', 'created_by', 'createdBy'],
      dateStart: ['ds', 'date_start', 'datestart', 'datetime', 'date_time'],
      dateEnd: ['de', 'date_end', 'dateend'],
      dateType: ['dt', 'date_type', 'datetype', 'dtype', 'date_interval', 'dateinterval', 'dinterval', 'date_trunc', 'datetrunc', 'dtrunc'],
      DRD : ['d', 'drd'],
      database : ['db', 'dtbase'],
      dueDate : ['dd', 'due_date', 'dueDate'],
      description : ['desc'],
      estimatedTime : ['et', 'estimated_time', 'estimatedTime'],
      email : ['e', 'email'],
      frontEnd : ['fe', 'front_end', 'frontEnd'],
      infrastructure : ['i', 'infra'],
      language : ['language'],
      numberSo : ['no', 'number_so', 'numberSo'],
      name : ['n'],
      nameGroup : ['ng'],
      other : ['o', 'ot'],
      parameter: ['p', 'param', 'parameter'],
      proposal: ['propos'],
      projectPresentation : ['pp', 'projpresent', 'projectPresentation'],
      projectName : ['pn', 'projname', 'projectName'],
      projectOwner : ['po', 'projown', 'projectOwner'],
      progressReport : ['pr', 'progreport', 'progressReport'],
      progress : ['prog'],
      password : ['pass'],
      phone : ['phone'],
      role : ['r'],
      qualityControl : ['qc', 'quality_control'],
      qualityAsurance : ['qa', 'quality_asurance'], 
      //status = ['s', 'stat'],
      //startDate = ['sd', 'start_date', 'startDate'],
      tasksTypeId : ['tti', 'tasks_type_id', 'tasksTypeId'],
      tasksStatusId : ['tsi', 'tasks_status_id', 'tasksStatusId'],
      tasksPriorityId : ['tpi', 'tasks_priority_id', 'tasksPriorityId'],
      tasksLabelId : ['tli', 'tasks_label_id', 'tasksLabelId'],
      timePlan : ['tp', 'timeplan'],
      technicalDocumentation : ['td', 'techdoc', 'technicalDocumentation'],
      testScript : ['ts', 'testscript'],
      testScriptUat : ['tsu', 'testuat', 'testScriptUat'],
      targetGoLife : ['tgl', 'target_go_life'],
      uiUx : ['uu']
    }

    // copy query string to params
    // for (const key of Object.keys(reqParams)) {
    //   const param = Object.keys(paramChecks).filter(p => paramChecks[p].indexOf(key) > -1)[0] || key
    //   let value = reqParams[key]
    //   if (value == '') value = undefined
    //   if (!params.hasOwnProperty(param)) params[param] = value
    // }

    // date handling

    // const format = {
    //   hour: ['YYYYMMDDHH', 'YYYY-MM-DD-HH'],
    //   day: ['YYYYMMDD', 'YYYY-MM-DD'],
    //   week: ['GGGGWW'],
    //   month: ['YYYYMM', 'YYYY-MM'],
    //   year: ['YYYY']
    // }

    // let { dateType = 'day' } = params
    // dateType = dateType.toLowerCase()
    // const { dateStart = moment().format(formats['day'][0]),
    //   dateEnd = dateStart,
    //   dateNearest = moment(dateStart, formats[dateType]).format() } = params
    // params.dateType = dateType
    // params.dateStart = dateStart
    // params.dateEnd = dateEnd
    // params.dateNearest = dateNearest
    // params.weekNearest = moment(dateNearest).format(formats['week'][0])

    // convert to array
    // const { finder, search, sorter, order, relations } = params
    // if (finder && !Array.isArray(finder)) params.finder = [ finder ]
    // if (search && !Array.isArray(search)) params.search = [ search ]
    // if (relations && !Array.isArray(relations)) params.relations = [ relations ]
    //if (workspace) params.schema = Model.schema(params.workspace)

    params.finders = params.finder
    params.searches = params.search

    // remove additional string (ex: 2g -> 2)

  //   params.techs.forEach((tech, idx) => {
  //     if (!Helpers.Number.isInt(tech)) params.techs[idx] = tech.substr(0, 1)
  //   })
  // }

  // handleDownStream ({ request, response, params, view }) {
  //   debug('handleDownStream')

  //   const { output } = params
  //   if (output) {
  //     switch (request.format()) {
  //       case 'json':
  //         response.status(200).json(output)
  //         break
  //       default:
  //       response.send(view.render('plain', { jsonSource: JSON.stringify(output) }))
  //     }
  //   }
  }
  
}

module.exports = Param
