"use strict"

const User = use("App/Models/User")

const Mail = use('Mail')


class AuthController {
  async register({ request, auth, response }) {
    let user = await User.create(request.all())

    let token = await auth.generate(user)

    Object.assign(user, token)

    return response.json(user)
  }

  async login({ request, auth, response }) {
    let { email, password } = request.all()
    console.log(email, password);
    try {
      if (await auth.attempt(email,password)) {
        let user = await User.findBy("email",email)
        let token = await auth.generate(user)

        Object.assign(user, token)
        return response.json(user)
        
      }
    } catch (e) {
      console.log(e)
      return response.json({ message: "You are not registered!" })
    }
  }

  async forgotPassword({request,response}){
    console.log("send email")
    try {
        await Mail.raw('<h1> HTML email </h1>' , (message) => {
          message.from('demo.tracker01@gmail.com')
          message.to('mhmmdakbr30@gmail.com')
        })
        
    } catch (e) {
      console.log(e)
      return response.json({ message: "Something Wrong!" })
    } 
  }
}

module.exports = AuthController
