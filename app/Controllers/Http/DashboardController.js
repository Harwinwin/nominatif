'use strict'
const DB = use('Database')

class DashboardController {

  async getDataByMonth({ response, request, params }) {

    const data = await DB.raw(`select c.bulan,
        case c.bulan
          when 'January' then January
          when 'February' then February
          when 'March' then March
              when 'April' then April
          when 'May' then May
          when 'June' then June
              when 'July' then July
          when 'August' then August
          when 'September' then September
              when 'October' then October
          when 'November' then November
          when 'December' then December
        end as total_data
      from (
      SELECT  
          SUM(CASE MONTH(tanggal) WHEN 1 THEN 1 ELSE 0 END) AS January,
        SUM(CASE MONTH(tanggal) WHEN 2 THEN 1 ELSE 0 END) AS February,
        SUM(CASE MONTH(tanggal) WHEN 3 THEN 1 ELSE 0 END) AS March,
        SUM(CASE MONTH(tanggal) WHEN 4 THEN 1 ELSE 0 END) AS April,
        SUM(CASE MONTH(tanggal) WHEN 5 THEN 1 ELSE 0 END) AS May,
        SUM(CASE MONTH(tanggal) WHEN 6 THEN 1 ELSE 0 END) AS June,
        SUM(CASE MONTH(tanggal) WHEN 7 THEN 1 ELSE 0 END) AS July,
        SUM(CASE MONTH(tanggal) WHEN 8 THEN 1 ELSE 0 END) AS August,
        SUM(CASE MONTH(tanggal) WHEN 9 THEN 1 ELSE 0 END) AS September,
        SUM(CASE MONTH(tanggal) WHEN 10 THEN 1 ELSE 0 END) AS October,
        SUM(CASE MONTH(tanggal) WHEN 11 THEN 1 ELSE 0 END) AS November,
        SUM(CASE MONTH(tanggal) WHEN 12 THEN 1 ELSE 0 END) AS December
      FROM entertain
      WHERE YEAR(tanggal) = ${request.params.year} AND isDeleted = 0
      ) as a
      cross join
      (
        select 'January' as bulan
        union all select 'February'
        union all select 'March'
          union all select 'April'
        union all select 'May'
          union all select 'June'
          union all select 'July'
        union all select 'August'
          union all select 'September'
        union all select 'October'
          union all select 'November'
          union all select 'December'
      ) c
      `)
    response.send({ status: "success", result: data[0] })
  }

  async getDataByStatus({ response, request, params }) {

    const data = await DB.raw(`SELECT  
        SUM(CASE status WHEN 1 THEN 1 ELSE 0 END) AS 'Waiting',
      SUM(CASE status WHEN 2 THEN 1 ELSE 0 END) AS 'Accepted',
      SUM(CASE status WHEN 3 THEN 1 ELSE 0 END) AS 'Rejected'
    FROM entertain
    WHERE YEAR(tanggal) = 2019 AND isDeleted = 0`)
    response.send({ status: "success", result: data[0] })
  }

  async getDataByStatus({ response, request, params }) {

    const data = await DB.raw(`SELECT  
      SUM(CASE status WHEN 1 THEN 1 ELSE 0 END) AS 'Waiting',
    SUM(CASE status WHEN 2 THEN 1 ELSE 0 END) AS 'Accepted',
    SUM(CASE status WHEN 3 THEN 1 ELSE 0 END) AS 'Rejected'
  FROM entertain
  WHERE YEAR(tanggal) = 2019 AND isDeleted = 0`)
    response.send({ status: "success", result: data[0] })
  }

}

module.exports = DashboardController
