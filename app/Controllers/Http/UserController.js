'use strict'
const DB = use('Database')
const Hash = use('Hash')
class UserController {

    async getuserViewAll({response, request,params}){
        // return id, group, name, email

        const userview = await DB.raw(`SELECT u.id as ID,rl.role_name as Role, u.user_name as Name, u.email as Email , dv.divisi_name FROM users u
                                    LEFT JOIN Role rl ON u.role = rl.id
                                    LEFT JOIN Divisi dv ON u.divisi = dv.id
                                    WHERE isDeleted = 0
                                    ORDER BY u.id
                            `)
        return userview;
    }

    async getuserbyID({response, request,params}){
        // return id, group, name, email
        const userview = await DB.raw(`SELECT u.id as ID,rl.role_name as Role, u.user_name as Name, u.email as Email , dv.divisi_name , u.password FROM users u
                                    LEFT JOIN Role rl ON u.role = rl.id
                                    LEFT JOIN Divisi dv ON u.divisi = dv.id
                                    WHERE u.id = ${params.id}
                            `)
        return userview;
    }

    async getUserAtasan({response, request,params}){
        // return id, group, name, email

        const atasan = await DB.raw(`SELECT u.user_name FROM users u
                                    WHERE u.role = 3 AND u.divisi = ${params.divisi}`)
        return atasan[0][0];
    }

    async updateProfileByID({request, response, params}) {
        const { Name,Email,Password,userId} = await request.post();
        console.log(Password) 

        if(Password != null){
            
            console.log("Jalan1")
            const passEncrypt = await Hash.make(Password)
            const isSame = await Hash.verify(Password, passEncrypt)
            await console.log(passEncrypt)
           
           if(isSame){
               await console.log(`'${Name}', '${Email}', '${Password}'`)
               const updateuser =  await DB.raw(`
                                       UPDATE users SET 
                                       user_name = '${Name}',
                                       password = '${passEncrypt}',
                                       email = '${Email}'
                                       WHERE id = ${userId}`)
               console.log(updateuser)
   
               var resp;
   
               if(updateuser){
                   resp = {status : "success" , message : "User Created"}
                   response.status(200).send(JSON.stringify(resp))
                   
               }else{
                   resp = {status : "failed" , message : "Something Wrong"}
                   response.status(500).send(JSON.stringify(resp))
               }
           }else{
               response.status(500).send("Ada Kesalahan");
           }

        }else if(Password == null){
            console.log("Jalan3")

            const updateuser =  await DB.raw(`
            UPDATE users SET 
            user_name = '${Name}',
            email = '${Email}'
            WHERE id = ${userId}`)
                console.log(updateuser)

                var resp;

                if(updateuser){
                resp = {status : "success" , message : "User Created"}
                response.status(200).send(JSON.stringify(resp))

                }else{
                resp = {status : "failed" , message : "Something Wrong"}
                response.status(500).send(JSON.stringify(resp))
                }
        }

    }

    async getDataInput({response, request,params}){
        // return id, group, name, email

        const role = await DB.raw(`SELECT r.id, r.role_name FROM Role r`)
        const divisi = await DB.raw(`SELECT dv.id, dv.divisi_name FROM Divisi dv`)
        
        return {role:role[0], divisi:divisi[0]}
    }

    
    async postaddNewuser({response, request, params, views}){
        // return role, fullName, password, email
         const { Divisi, Name, Email, Role, Password} = await request.post();
            const passEncrypt = await Hash.make(Password)
            const isSame = await Hash.verify(Password, passEncrypt)
            await console.log(passEncrypt)
           
           if(isSame){
                   await console.log(`'${Divisi}', '${Name}', '${Email}', '${Role}', '${Password}'`)
               
                  const newuser = await DB.raw(`INSERT INTO users (divisi, user_name, email, role, password, isDeleted)
                                       VALUES ('${Divisi}', '${Name}', '${Email}', '${Role}', '${passEncrypt}', 0)`)
               console.log(newuser)
   
               var resp;
   
               if(newuser){
                   resp = {status : "success" , message : "User Created"}
                   response.status(200).send(JSON.stringify(resp))
                   
               }else{
                   resp = {status : "failed" , message : "Something Wrong"}
                   response.status(500).send(JSON.stringify(resp))
               }
           }else{
               response.status(500).send("Ada Kesalahan");
           }         
        }
    
    async getUserDetail({response, request,params}){
        // return id, group, name, email

        const userview = await DB.raw(`SELECT u.id as ID,u.role as Role, u.user_name as Name, u.email as Email , u.divisi , u.password FROM users u
                                    LEFT JOIN Role rl ON u.role = rl.id
                                    LEFT JOIN Divisi dv ON u.divisi = dv.id
                                    WHERE u.id = ${params.id}
                            `)
        return userview;
    }

    async updateUserByID({request, response, params}) {
        const { Name,Email,Password,userId, Divisi, Role} = await request.post();
        console.log(Password) 

        if(Password != null){
            
            console.log("Jalan1")
            const passEncrypt = await Hash.make(Password)
            const isSame = await Hash.verify(Password, passEncrypt)
            await console.log(passEncrypt)
           
           if(isSame){
               await console.log(`'${Name}', '${Email}', '${Password}'`)
               const updateuser =  await DB.raw(`
                                       UPDATE users SET 
                                       user_name = '${Name}',
                                       password = '${passEncrypt}',
                                       email = '${Email}',
                                       divisi = '${Divisi}',
                                       role = '${Role}'
                                       WHERE id = ${userId}`)
               console.log(updateuser)
   
               var resp;
   
               if(updateuser){
                   resp = {status : "success" , message : "User Created"}
                   response.status(200).send(JSON.stringify(resp))
                   
               }else{
                   resp = {status : "failed" , message : "Something Wrong"}
                   response.status(500).send(JSON.stringify(resp))
               }
           }else{
               response.status(500).send("Ada Kesalahan");
           }

        }else if(Password == null){
            console.log("Jalan3")

            const updateuser =  await DB.raw(`
            UPDATE users SET 
            user_name = '${Name}',
            email = '${Email}',
            divisi = '${Divisi}',
            role = '${Role}'
            WHERE id = ${userId}`)
                console.log(updateuser)

                var resp;

                if(updateuser){
                resp = {status : "success" , message : "User Created"}
                response.status(200).send(JSON.stringify(resp))

                }else{
                resp = {status : "failed" , message : "Something Wrong"}
                response.status(500).send(JSON.stringify(resp))
                }
        }

    }

    async deleteUser({ params, response, request }) {
        const {ID} = request.post();
        console.log(ID)
        const deleteuser = await DB.raw(`
        UPDATE users
        SET isDeleted = 1
        WHERE id = ${ID}`)

        console.log(deleteuser)
    
        var resp;
    
        if (deleteuser) {
            resp = { status: "success", message: "User Deleted" }
            response.status(200).send(JSON.stringify(resp))
        } else {
            resp = { status: "failed", message: "Something Wrong" }
            response.status(500).send(JSON.stringify(resp))
            }
    }  
}

module.exports = UserController
