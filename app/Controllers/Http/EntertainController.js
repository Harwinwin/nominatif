'use strict'
const DB = use('Database')


class EntertainController {

    async getAllEntertain({ params, response, request }) {
        if (params.roleId == 1) {
            const entertain = await DB.raw(`SELECT e.*, st.status_name as status_name, u.user_name as pic, DATE_FORMAT(e.tanggal, "%Y-%m-%d") as date,
            dv.divisi_name as Divisi
            FROM Entertain e
            INNER JOIN status_groups st ON e.status = st.id
            INNER JOIN  users u ON e.created_by = u.id
            INNER JOIN Divisi dv ON u.divisi = dv.id
            WHERE e.isDeleted = 0
            ORDER BY e.id`)

            return entertain;
        } else if (params.roleId == 3) {
            const entertain = await DB.raw(`SELECT e.*, st.status_name as status_name, u.user_name as pic, DATE_FORMAT(e.tanggal, "%Y-%m-%d") as date,
            dv.divisi_name as Divisi
            FROM Entertain e
            INNER JOIN status_groups st ON e.status = st.id
            INNER JOIN  users u ON e.created_by = u.id
            INNER JOIN Divisi dv ON u.divisi = dv.id
            WHERE u.divisi = ${params.divisiId} AND e.isDeleted = 0
            ORDER BY e.id`)

            return entertain;
        } else if (params.roleId == 2) {
            const entertain = await DB.raw(`SELECT e.*, st.status_name as status_name, u.user_name as pic, DATE_FORMAT(e.tanggal, "%Y-%m-%d") as date,
            dv.divisi_name as Divisi
            FROM Entertain e
            INNER JOIN status_groups st ON e.status = st.id
            INNER JOIN  users u ON e.created_by = u.id
            INNER JOIN Divisi dv ON u.divisi = dv.id
            WHERE e.created_by = ${params.userId} AND e.isDeleted = 0
            ORDER BY e.id`)

            return entertain;
        }
    }

    async getEntertaintById({params}){
        const entertaint = await DB.raw(`SELECT e.*, st.status_name as status_name, u.user_name as pic, DATE_FORMAT(e.tanggal, "%Y-%m-%d") as date,
        dv.divisi_name as Divisi
        FROM Entertain e
        LEFT JOIN status_groups st ON e.status = st.id
        LEFT JOIN  users u ON e.created_by = u.id
        LEFT JOIN Divisi dv ON u.divisi = dv.id
        WHERE e.id = ${params.id}`)

        return entertaint
    }

    async updateEntertaint({request, response, params}){
        const {Tanggal, Jenis, Via, No_Trx, Project_name, Penerima, Nominal, PIC, Perusahaan, Periode, SelectedID} = await request.post();
        
        const editEntertaint = await DB.raw(`UPDATE Entertain SET
                                            tanggal = '${Tanggal}',
                                            jenis = '${Jenis}',
                                            pengeluaran = '${Via}',
                                            no_trx = '${No_Trx}',
                                            projek_name = '${Project_name}',
                                            penerima = '${Penerima}',
                                            nominal = '${Nominal}',
                                            created_by = '${PIC}',
                                            perusahaan = '${Perusahaan}',
                                            periode_cc = '${Periode}'
                                            WHERE id = '${SelectedID}'
        `)

        var resp

        if(editEntertaint){
            resp = {status : "success", message : "Entertaint Change"}
        } else {
            resp = {status : "failed", message : "Something Wrong"}
        }

        response.status(200).send(JSON.stringify(resp))
    }
    
    async getLatestEntertain({ params, response, request }) {
      
            const entertain = await DB.raw(`SELECT e.*, st.status_name as status_name, u.user_name as pic, DATE_FORMAT(e.tanggal, "%d/%m/%Y") as date,
            dv.divisi_name as Divisi
            FROM Entertain e
            LEFT JOIN status_groups st ON e.status = st.id
            LEFT JOIN  users u ON e.created_by = u.id
            LEFT JOIN Divisi dv ON u.divisi = dv.id
            WHERE e.isDeleted = 0 AND e.status = 2
            ORDER BY e.id DESC 
            LIMIT 1`)

            return entertain[0];
    }

    async addEntertain({ params, response, request }) {
        const { Tanggal, Jenis,Periode, Via, No_Trx, Project_name, Penerima, PIC, status, Perusahaan, Nominal } = await request.post();

        const entertain = await DB.raw(`INSERT INTO Entertain (tanggal, jenis, pengeluaran, no_trx, projek_name,penerima,nominal,status,created_by,perusahaan,periode_cc, isDeleted)
        VALUES ('${Tanggal}', '${Jenis}', '${Via}', '${No_Trx}', '${Project_name}', '${Penerima}', '${Nominal}', '${status}', '${PIC}', '${Perusahaan}', '${Periode}', 0 )`)

        console.log(entertain)

        var resp;

        if (entertain) {
            resp = { status: "success", message: "Entertaint Created" }
            response.status(200).send(JSON.stringify(resp))

        } else {
            resp = { status: "failed", message: "Something Wrong" }
            response.status(500).send(JSON.stringify(resp))
        }
    }

    async changeStatus({ params, response, request }) {
        const { ID,Status } = await request.post();
        if(Status === "approved"){
                const changetstatus = await DB.raw(`
            UPDATE Entertain
            SET status = 2
            WHERE id = ${ID}`)

            console.log(changetstatus)

            var resp;

            if (changetstatus) {
                resp = { status: "success", message: "Entertaint Approved" }
                response.status(200).send(JSON.stringify(resp))

            } else {
                resp = { status: "failed", message: "Something Wrong" }
                response.status(500).send(JSON.stringify(resp))
            }
        }else if(Status === "rejected"){
                const changetstatus  = await DB.raw(`
            UPDATE Entertain
            SET status = 3
            WHERE id = ${ID}`)

            console.log(changetstatus)

            var resp;

            if (changetstatus) {
                resp = { status: "success", message: "Entertaint Rejected" }
                response.status(200).send(JSON.stringify(resp))

            } else {
                resp = { status: "failed", message: "Something Wrong" }
                response.status(500).send(JSON.stringify(resp))
            }
        }
    }

        async deleteEntertaint({ params, response, request }) {
            const {ID} = await request.post();
            console.log(ID)
            const deleteentertaint = await DB.raw(`
            UPDATE Entertain
            SET isDeleted = 1
            WHERE id = ${ID}`)
    
                console.log(deleteentertaint)
    
                var resp;
    
                if (deleteentertaint) {
                    resp = { status: "success", message: "Entertaint Deleted" }
                    response.status(200).send(JSON.stringify(resp))
    
                } else {
                    resp = { status: "failed", message: "Something Wrong" }
                    response.status(500).send(JSON.stringify(resp))
                } 
        }      
}

module.exports = EntertainController
